
(* An example: Add two numbers and display the result *)

open Wd_dialog
open Wd_run_cgi
open Wd_types


class add_numbers universe name env =
object (self)
  inherit dialog universe name env

  method prepare_page() =
    (* This method is empty in this example *)
    ()

  method handle() =
    (* Check which event has happened: *)
    match self # event with
	Button("add") ->
	  (* Get the numbers and convert them from string to int. Catch
	   * errors.
	   *)
	  let n_1, n_2 =
	    ( try
		let s_1 = self # string_variable "first_number" in
		let s_2 = self # string_variable "second_number" in
		(int_of_string s_1, int_of_string s_2)
	      with
		  error ->
		    (* On error: Jump to the error page *)
		    raise(Change_page "display-error")
	    )
	  in
	  (* Add the numbers, and store the result into the variable *)
	  let r = n_1 + n_2 in
	  self # set_variable "result" (String_value (string_of_int r));

      | _ ->
	  (* Do nothing if the event is not recognized *)
	  ()
end
;;


run
  (* ~charset:`Enc_utf8 *)
  ~reg:(fun universe ->
	  (* Bind the XML dialog "add-numbers" to the class "add_numbers": *)
          universe # register "add-numbers" (new add_numbers)
       )
  ()
;;

