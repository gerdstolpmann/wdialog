# $Id: Obj.pm,v 3.1 2002-02-13 00:10:17 stolpmann Exp $
# ----------------------------------------------------------------------
#

package Obj;
use strict;

use UI::Object;
use vars qw(@ISA);

@ISA = ("UI::Object");


sub handle {
    my ($self) = @_;

    my ($filename, $mimetype, $sysfile) = $self->uploaded_file('upload');
    $self->set_string_variable('filename', $filename);
    $self->set_string_variable('mimetype', $mimetype);
    $self->set_string_variable('tmpfile',  $sysfile);

    return undef;
}


sub prepare_page {
}

1;

# ======================================================================
# History:
# 
# $Log: Obj.pm,v $
# Revision 3.1  2002-02-13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.1  2000/04/17 10:14:17  gerd
# 	Initial revision.
#
# Revision 1.1  2000/04/13 17:43:11  gerd
# 	Initial revision.
#
# 
