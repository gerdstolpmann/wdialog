# $Id: Obj1.pm,v 3.1 2002-02-13 00:10:17 stolpmann Exp $
# ----------------------------------------------------------------------
#

package Obj1;
use strict;

use UI::Object;
use UI::Universe;
use UI::Variable;
use Obj2;
use vars qw(@ISA);

@ISA = ("UI::Object");

sub handle {
    my ($self) = @_;

    if ($self->page_name() eq 'page1') {
	my @e = $self->event();
	if ($e[0] eq 'BUTTON' && $e[1] eq 'call') {
	    my $obj2 = UI::Universe::get("obj2");
	    $obj2->set_variable("save",
				new UI::Variable::Object($self));
	    return $obj2;
	}
    }
}


sub prepare_page {
}

1;

# ======================================================================
# History:
# 
# $Log: Obj1.pm,v $
# Revision 3.1  2002-02-13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.1  2000/04/13 17:43:15  gerd
# 	Initial revision.
#
# 
