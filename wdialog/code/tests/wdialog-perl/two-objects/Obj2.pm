# $Id: Obj2.pm,v 3.1 2002-02-13 00:10:17 stolpmann Exp $
# ----------------------------------------------------------------------
#

package Obj2;
use strict;

use UI::Object;
use UI::Universe;
use UI::Variable;
use Obj1;
use vars qw(@ISA);

@ISA = ("UI::Object");

sub handle {
    my ($self) = @_;
    my $page = $self->page_name();
    my @e = $self->event();
    if ($page eq 'page1') {
	if ($e[0] eq 'BUTTON' && $e[1] eq 'return') {
	    my $obj1 = $self->variable("save")->value;
	    $obj1;
	};
    };
}


sub prepare_page {
    my ($self) = @_;
    my $obj1 = $self->variable("save")->value;
    $self->set_string_variable("w", $obj1->string_variable("v"));
}

1;

# ======================================================================
# History:
# 
# $Log: Obj2.pm,v $
# Revision 3.1  2002-02-13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.1  2000/04/13 17:43:15  gerd
# 	Initial revision.
#
# 
