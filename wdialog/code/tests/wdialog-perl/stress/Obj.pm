# $Id: Obj.pm,v 3.2 2002-02-28 18:52:29 stolpmann Exp $
# ----------------------------------------------------------------------
#

package Obj;
use strict;

use UI::Object;
use UI::Template;
use vars qw(@ISA);

@ISA = ("UI::Object");


sub handle {
}


sub prepare_page {
    my ($self) = @_;

    my $rs = "Wert von s: " . $self->string_variable('s');

    my @e = $self->enum_variable('e');
    my $re = "Wert von e: " . join(',', @e);

    my $d = $self->variable('d');
    my $rd = "Wert von d: ";
    $d->iter( sub {
		  my ($i,$e) = @_;
		  $rd .= "($i,$e)";
	      });

    my $a = $self->variable('a');
    my $ra = "Wert von a: ";
    $a->iter( sub {
		  my ($i,$v) = @_;
		  my $s = $v->value();
		  $ra .= "($i,$s)";
	      });

    $self->set_string_variable('report', "$rs\n$re\n$rd\n$ra");

    # Dumb stuff to test GC behaviour:
    $self->set_variable('s', new UI::Variable::String("huhuhuhuhu"));
    $self->set_variable('e', new UI::Variable::Enum('a','d'));

    my $t = UI::Template->get('testtemplate');
    my $y = UI::Template->apply($t, { 'testparam' => UI::Template->text("HUHU") });
}

1;

# ======================================================================
# History:
#
# $Log: Obj.pm,v $
# Revision 3.2  2002-02-28 18:52:29  stolpmann
# 	updated to newest perlapi
#
# Revision 3.1  2002/02/13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.1  2002/01/22 17:49:20  gerd
# 	Initial revision (as 'stress')
#
# Revision 1.1  2000/04/13 17:43:11  gerd
# 	Initial revision.
#
#
