#! /usr/local/bin/perl

# $Id: index.cgi,v 3.1 2002-02-13 00:10:17 stolpmann Exp $
# ----------------------------------------------------------------------
#

BEGIN {
    # Test phase:
    use lib '../../../src/UI/blib/lib';
    use lib '../../../src/UI/blib/arch';
}


use UI::Stubs;
use UI::Object;
use Obj;

use strict;

$|=1;

eval {
   my $x = Obj->register("obj1");
   for (my $n=1; $n <= 10000; $n++) {
       print "-------------------------------- n=$n ------------------------------\n";
       UI::Stubs::work();
   }
};

if ($@) {
    print "Content-type: text/plain\n\n";
    print $@;
}

exit(0);


# ======================================================================
# History:
#
# $Log: index.cgi,v $
# Revision 3.1  2002-02-13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.1  2002/01/22 17:49:20  gerd
# 	Initial revision (as 'stress')
#
# Revision 1.2  2000/04/17 11:29:40  gerd
# 	Updated
#
# Revision 1.1  2000/04/13 17:43:12  gerd
# 	Initial revision.
#
#
