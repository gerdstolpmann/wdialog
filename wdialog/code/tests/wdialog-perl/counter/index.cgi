#! /usr/local/bin/perl

# $Id: index.cgi,v 3.1 2002-02-13 00:10:17 stolpmann Exp $
# ----------------------------------------------------------------------
#


BEGIN {
    # Test phase:
    $DB::single = 1;
    use lib '../../../src/UI/blib/lib';
    use lib '../../../src/UI/blib/arch';
};


use UI::Stubs;
use UI::Object;
use Counter;

use strict;

eval {
    UI::Stubs::init('Back_button', 1);
    my $x = Counter->register("obj1");
    UI::Stubs::work();
};

if ($@) {
    print "Content-type: text/plain\n\n";
    print $@;
}

exit(0);



# ======================================================================
# History:
#
# $Log: index.cgi,v $
# Revision 3.1  2002-02-13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.3  2000/12/13 16:41:36  gerd
# 	Test: Back button switch
#
# Revision 1.2  2000/04/17 11:28:22  gerd
# 	Updated.
#
# Revision 1.1  2000/04/13 17:43:10  gerd
# 	Initial revision.
#
#
