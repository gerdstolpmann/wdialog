# $Id: Counter.pm,v 3.1 2002-02-13 00:10:17 stolpmann Exp $
# ----------------------------------------------------------------------
#

package Counter;
use strict;

use UI::Dialog;
use vars qw(@ISA);

@ISA = ("UI::Dialog");


sub handle {
    my ($self) = @_;
    my ($e, $name) = $self->event;
    if ($e eq 'BUTTON' && $name eq 'count') {
	$self->set_string_variable('v1', $self->string_variable('v1') + 1);
    };
    return undef;
}


sub prepare_page {
}

1;

# ======================================================================
# History:
# 
# $Log: Counter.pm,v $
# Revision 3.1  2002-02-13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.1  2000/04/13 17:43:10  gerd
# 	Initial revision.
#
# 
