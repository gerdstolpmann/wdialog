# $Id: Obj.pm,v 3.1 2002-02-13 00:10:17 stolpmann Exp $
# ----------------------------------------------------------------------
#

package Obj;
use strict;

use UI::Object;
use vars qw(@ISA);

@ISA = ("UI::Object");


sub handle {
}


sub prepare_page {
    my ($self) = @_;

    my $rs = "Wert von s: " . $self->string_variable('s');

    my @e = $self->enum_variable('e');
    my $re = "Wert von e: " . join(',', @e);

    my $d = $self->variable('d');
    my $rd = "Wert von d: ";
    $d->iter( sub {
	my ($i,$e) = @_;
	$rd .= "($i,$e)";
    });

    my $a = $self->variable('a');
    my $ra = "Wert von a: ";
    $a->iter( sub {
	my ($i,$v) = @_;
	my $s = $v->value();
	$ra .= "($i,$s)";
    });

    $self->set_string_variable('report', "$rs\n$re\n$rd\n$ra");
}

1;

# ======================================================================
# History:
# 
# $Log: Obj.pm,v $
# Revision 3.1  2002-02-13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.1  2000/04/13 17:43:11  gerd
# 	Initial revision.
#
# 
