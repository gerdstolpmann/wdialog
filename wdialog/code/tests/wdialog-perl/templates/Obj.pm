# $Id: Obj.pm,v 3.2 2002-02-28 18:52:29 stolpmann Exp $
# ----------------------------------------------------------------------
#

package Obj;
use strict;

use UI::Object;
use UI::Template;
use vars qw(@ISA);

@ISA = ("UI::Object");


sub handle {
}


sub prepare_page {
    my ($self) = @_;

    my $tm1 = UI::Template->get("tm");
    my $tm2 = UI::Template->get("tm");

    my $x = UI::Template->apply($tm1,
				 { 'x' => UI::Template->text("X"),
				  'y' => UI::Template->text("Y")
				  });

    my $y = UI::Template->apply($tm1,
				{ 'x' => UI::Template->text("A"),
				  'y' => UI::Template->text("B")
				  });

    my $z = UI::Template->concat(UI::Template->empty(),
				 [ $x, $y ]);

    my $s = $z->to_string($self);

    $self->set_string_variable('report', "$s");
}

1;

# ======================================================================
# History:
#
# $Log: Obj.pm,v $
# Revision 3.2  2002-02-28 18:52:29  stolpmann
# 	updated to newest perlapi
#
# Revision 3.1  2002/02/13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.2  2002/01/21 14:26:11  gerd
# 	Updated to DTD Version 2
#
# Revision 1.1  2000/04/13 17:43:13  gerd
# 	Initial revision.
#
#
