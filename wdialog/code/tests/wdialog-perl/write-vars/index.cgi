#! /usr/local/bin/perl

# $Id: index.cgi,v 3.1 2002-02-13 00:10:17 stolpmann Exp $
# ----------------------------------------------------------------------
#

BEGIN {
    # Test phase:
    use lib '../../../src/UI/blib/lib';
    use lib '../../../src/UI/blib/arch';
}


use UI::Stubs;
use UI::Object;
use Obj;

use strict;

eval {
   my $x = Obj->register("obj1");
   UI::Stubs::work();
};

if ($@) {
    print "Content-type: text/plain\n\n";
    print $@;
}

exit(0);


# ======================================================================
# History:
#
# $Log: index.cgi,v $
# Revision 3.1  2002-02-13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.2  2000/04/17 11:28:29  gerd
# 	Updated.
#
# Revision 1.1  2000/04/13 17:43:17  gerd
# 	Initial revision.
#
#
