# $Id: Obj.pm,v 3.1 2002-02-13 00:10:17 stolpmann Exp $
# ----------------------------------------------------------------------
#

package Obj;
use strict;

use UI::Object;
use vars qw(@ISA);

@ISA = ("UI::Object");


sub handle {
}


sub prepare_page {
    my ($self) = @_;

    my $sv = new UI::Variable::String("X");
    my $ev = new UI::Variable::Enum("a", "c", "d");
    my $dv = new UI::Variable::DynEnum(["1", "A"], ["3", "B"]);
    my $av = new UI::Variable::Alist(["1", new UI::Variable::String("X")],
				     ["2", new UI::Variable::String("Y")],
				     ["3", new UI::Variable::String("Z")]);
    $self->set_variable("s", $sv);
    $self->set_variable("e", $ev);
    $self->set_variable("d", $dv);
    $self->set_variable("a", $av);
}

1;

# ======================================================================
# History:
# 
# $Log: Obj.pm,v $
# Revision 3.1  2002-02-13 00:10:17  stolpmann
# 	Initial revision at sourceforge.
#
# Revision 1.1  2000/04/13 17:43:17  gerd
# 	Initial revision.
#
# 
