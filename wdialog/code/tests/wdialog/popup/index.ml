(* $Id: index.ml,v 3.1 2002-02-12 21:28:19 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types

class obj1 universe name env =
  object (self)
    inherit Wd_dialog.dialog universe name env

    method prepare_page() =
      self # set_variable
	"dv"
	(Dyn_enum_value [ "1", "Eins"; "2", "Zwei"; "3", "Drei" ]);
      ()

    method handle() =
      match self # event with
	  No_event -> ()
	| Button "count" ->
	    self # set_variable
	      "v1"
	      (String_value
		 (string_of_int
		    (int_of_string (self # string_variable "v1") + 1)))
	| Button "reset" ->
	    self # set_variable "v1" (String_value "0")
	| _ -> ()

  end
;;


Wd_run_cgi.run
  ~reg:(fun universe ->
          universe # register "obj1" (new obj1)
       )
  ()
;;


(* ======================================================================
 * History:
 *
 * $Log: index.ml,v $
 * Revision 3.1  2002-02-12 21:28:19  stolpmann
 * 	Initial revision at sourceforge
 *
 * Revision 1.2  2002/01/14 15:06:50  gerd
 * 	Updated to recent wd version.
 *
 * Revision 1.1  2000/09/25 13:22:56  gerd
 * 	Initial revsion
 *
 * Revision 1.1  2000/04/13 17:43:02  gerd
 * 	Initial revision.
 *
 *
 *)
