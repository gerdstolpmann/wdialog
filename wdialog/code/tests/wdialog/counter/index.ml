(* $Id: index.ml,v 3.1 2002-02-12 21:26:34 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_dialog
open Wd_run_cgi
open Wd_types

class obj1 universe name env =
  object (self)
    inherit dialog universe name env

    method prepare_page() =
      self # set_variable
	"dv"
	(Dyn_enum_value [ "1", "Eins"; "2", "Zwei"; "3", "Drei" ]);
      ()

    method handle() =
      match self # event with
	  No_event -> ()
	| Button "count" ->
	    self # set_variable
	      "v1"
	      (String_value
		 (string_of_int
		    (int_of_string (self # string_variable "v1") + 1)))
	| Button "reset" ->
	    self # set_variable "v1" (String_value "0")
	| _ -> ()

  end
;;

(*
Unix.close Unix.stdin;
ignore(Unix.openfile "/tmp/in" [ Unix.O_RDONLY ] 0 );
*)

run
  ~charset:`Enc_utf8
  ~reg:(fun universe ->
	  universe # register "obj1" (new obj1)
       )
  ()
;;

(* ======================================================================
 * History:
 *
 * $Log: index.ml,v $
 * Revision 3.1  2002-02-12 21:26:34  stolpmann
 * 	Initial revision at sourceforge
 *
 * Revision 1.3  2002/02/05 18:51:49  gerd
 * 	Updated.
 *
 * Revision 1.2  2002/01/14 15:06:43  gerd
 * 	Updated to recent wd version.
 *
 * Revision 1.1  2000/04/13 17:43:02  gerd
 * 	Initial revision.
 *
 *
 *)
