(* $Id: index.ml,v 1.1 2006-03-08 16:05:02 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_dialog
open Wd_run_cgi
open Wd_types

class obj1 universe name env =
  object (self)
    inherit dialog universe name env

    method prepare_page() =
      ()

    method handle() =
      match self # event with
	  No_event -> ()
	| Button "count" ->
	    self # set_variable
	      "v1"
	      (String_value
		 (string_of_int
		    (int_of_string (self # string_variable "v1") + 1)))
	| _ -> ()

  end
;;

(*
Unix.close Unix.stdin;
ignore(Unix.openfile "/tmp/in" [ Unix.O_RDONLY ] 0 );
*)

run
  ~charset:`Enc_utf8
  ~reg:(fun universe ->
	  universe # register "obj1" (new obj1)
       )
  ()
;;

(* ======================================================================
 * History:
 *
 * $Log: index.ml,v $
 * Revision 1.1  2006-03-08 16:05:02  stolpmann
 * Limited support for catalogs:
 *
 * The syntax <ui:catalog>...</ui:catalog> is supported, and it is
 * possible to define catalogs in ui files.
 *
 * The syntax $[m(token)] looks up the token in the current catalog.
 *
 * The function cat-translate is also available.
 *
 * ui:select has the "display" attribute.
 *
 * Revision 3.1  2002/02/12 21:26:34  stolpmann
 * 	Initial revision at sourceforge
 *
 * Revision 1.3  2002/02/05 18:51:49  gerd
 * 	Updated.
 *
 * Revision 1.2  2002/01/14 15:06:43  gerd
 * 	Updated to recent wd version.
 *
 * Revision 1.1  2000/04/13 17:43:02  gerd
 * 	Initial revision.
 *
 *
 *)
