(* $Id: index.ml,v 3.2 2006-03-08 00:56:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types

class obj1 universe name env =
object (self)
    inherit Wd_dialog.dialog universe name env

    method prepare_page() = ()

    method handle() =
      match self # event with
	  Button "call" ->
	    let obj2 = self # universe # create self#environment "obj2" in
	    obj2 # set_variable "save"
	      (Dialog_value (Some (self : #dialog_type :> dialog_type)));
	    raise (Change_dialog obj2)
	| _ -> ()

  end
;;


class obj2 universe name env =
object (self)
    inherit Wd_dialog.dialog universe name env

    method prepare_page() =
      self # set_variable "w" (String_value (self # string_variable "save.v"))

    method handle() =
      match self # event with
	  Button "return" ->
	    let obj1 =
	      match self # dialog_variable "save" with
		  None -> failwith "'save' not initialized"
	  	| Some o -> o
	    in
	    raise (Change_dialog obj1)
	| _ -> ()

  end
;;

Wd_run_cgi.run
  ~reg:(fun universe ->
          universe # register "obj1" (new obj1);
          universe # register "obj2" (new obj2);
       )
  ()
;;


(* ======================================================================
 * History:
 *
 * $Log: index.ml,v $
 * Revision 3.2  2006-03-08 00:56:45  stolpmann
 * Addition of Table_value, Matrix_value and Function_value.
 *
 * New parser for initial expressions. It is now possible to
 * use $[...] in variable initializers.
 *
 * Extended bracket expressions: functions, let expressions,
 * word literals, matrix literals.
 *
 * New type for message catalogs, although not yet implemented.
 *
 * Revision 3.1  2002/02/12 21:32:25  stolpmann
 * 	Initial revision at sourceforge
 *
 * Revision 1.2  2002/01/14 15:06:51  gerd
 * 	Updated to recent wd version.
 *
 * Revision 1.1  2000/04/13 17:43:06  gerd
 * 	Initial revision.
 *
 *
 *)
