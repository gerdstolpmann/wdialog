(* $Id: index.ml,v 3.1 2002-02-12 21:33:57 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types

class obj1 universe name env =
  object (self)
    inherit Wd_dialog.dialog universe name env

    method prepare_page() =
      ()

    method handle() =
      let filename, mimetype, tmpfile =
      match self # lookup_uploaded_file "upload" with
	  None ->
	    "NONE", "NONE", "NONE"
	| Some arg ->
	    ( match arg # filename with
		  None -> "NONE"
		| Some name -> name
	    ),
	    arg # content_type,
	    ( match arg # store with
		  `Memory -> "Memory"
		| `File name -> name
	    )
      in
      self # set_variable "filename" (String_value filename);
      self # set_variable "mimetype" (String_value mimetype);
      self # set_variable "tmpfile"  (String_value tmpfile);

  end
;;

Wd_run_cgi.run
  ~reg:(fun universe ->
          universe # register "obj1" (new obj1)
       )
  ()
;;


(* ======================================================================
 * History:
 *
 * $Log: index.ml,v $
 * Revision 3.1  2002-02-12 21:33:57  stolpmann
 * 	Initial revision at sourceforge
 *
 * Revision 1.2  2002/01/14 15:06:52  gerd
 * 	Updated to recent wd version.
 *
 * Revision 1.1  2000/04/17 10:13:37  gerd
 * 	Initial revision.
 *
 * Revision 1.1  2000/04/13 17:43:02  gerd
 * 	Initial revision.
 *
 *
 *)
