(* $Id: index.ml,v 3.1 2002-02-12 21:29:34 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types


class o universe name env =
  object (self)
    inherit Wd_dialog.dialog universe name env

    method prepare_page() = ()

    method handle() = ()

  end
;;


Wd_run_cgi.run
  ~reg:(fun universe ->
          universe # register "o" (new o)
       )
  ()
;;


(* ======================================================================
 * History:
 *
 * $Log: index.ml,v $
 * Revision 3.1  2002-02-12 21:29:34  stolpmann
 * 	Initial revision at sourceforge.
 *
 * Revision 1.1  2002/01/28 02:17:53  gerd
 * 	Initial revision in dir "shorthand"
 *
 * Revision 1.2  2002/01/14 15:06:46  gerd
 * 	Updated to recent wd version.
 *
 * Revision 1.1  2000/05/08 10:37:39  gerd
 * 	Initial revision.
 *
 *
 *)
