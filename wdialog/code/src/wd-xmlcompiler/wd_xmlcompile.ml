(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_xmlcompile.ml,v 3.3 2002-03-19 23:33:38 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

let main() =
  let file = ref "" in
  Arg.parse
      [ ]
      (fun s ->
	 file := s)
      "usage: wd-xmlcompile <file>.ui";

  if !file = "" then (
    prerr_endline "usage: wd-xmlcompile <file>.ui";
    exit 1;
  );

  let outfile = (!file ^ ".bin") in
  let out = open_out_bin outfile in
  try
    Wd_transform.compile !file out;
    close_out out;
  with
      error ->
	close_out out;
	prerr_endline ("Error:\n" ^ Pxp_types.string_of_exn error);
	Sys.remove outfile
;;

main();;

(* ======================================================================
 * History:
 *
 * $Log: wd_xmlcompile.ml,v $
 * Revision 3.3  2002-03-19 23:33:38  stolpmann
 * 	Fix in command-line parsing
 *
 * Revision 3.2  2002/02/14 16:16:45  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:36:31  stolpmann
 * 	Initial revision at sourceforge
 *
 * Revision 1.3  2000/12/13 16:41:06  gerd
 * 	Compiler now mainly in module Parse.
 *
 * Revision 1.2  2000/09/21 15:12:38  gerd
 * 	Updated for O'Caml 3 and PXP
 *
 * Revision 1.1  2000/04/13 17:43:19  gerd
 * 	Initial revision.
 *
 *
 *)
