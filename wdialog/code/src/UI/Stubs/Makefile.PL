# $Id: Makefile.PL,v 3.2 2002-06-04 10:32:24 stolpmann Exp $

use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.

$libdir=`ocamlfind query -format '%d' unix`;
$idldir=`ocamlfind query -format '%d' camlidl`;

# get the value of the 'sitearchexp' config variable:
$v='$' . `perl '-V:sitearchexp'`;
eval $v;

WriteMakefile(
	      'NAME'      => 'UI::Stubs',
	      'VERSION_FROM' => 'Stubs.pm', # finds $VERSION
              'OBJECT'    => 'Stubs.o',
	      'LIBS'      => ["-L$libdir -L$idldir -L../../ocamlrun-dso -L../../wdialog-perlguts
			         -locamlrun -lperlvalues -lcamlidl"],
	      'DEFINE'    => '',     # e.g., '-DHAVE_SOMETHING'
	      'INC'       => "-I$libdir",
	      'MAKEFILE'  => 'Makefile.perl',
	      #    'CCFLAGS'    => '-g',
	      );
