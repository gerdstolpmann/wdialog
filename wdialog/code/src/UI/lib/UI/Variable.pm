# <COPYRIGHT>
# Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH,
#                Gerd Stolpmann
#
# <GPL>
# This file is part of WDialog.
#
# WDialog is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# WDialog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with WDialog; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# </>
#

# $Id: Variable.pm,v 3.3 2002-03-05 16:22:27 stolpmann Exp $
# ----------------------------------------------------------------------
#

package UI::Variable;

use strict;
use vars qw($VERSION);

$VERSION = "0.0";

######################################################################

package UI::Variable::String;
use strict;

# Representation:
# The string is the object.

sub new_private {
    my ($class, $s) = @_;

    my $u = "$s";               # Make it a string, whatever it is.
    return bless(\$u, $class);
}


sub new {
    return new_private(@_);
}


sub type {
    my ($self) = @_;

    return "STRING";
}


sub value {
    my ($self) = @_;

    return $$self;
}


sub ocaml_repr {
    # Return the representation that the OCaml stubs can understand.
    return [ 0, shift @_ ];
}


sub this_object_is_an_UI_variable {
}



######################################################################

package UI::Variable::Enum;
use strict;

# Representation:
# List of internal values.
# The object is a blessed reference to this list.

sub new_private {
    my ($class, @internal_values) = @_;

    my $v = \@internal_values;

    return bless($v, $class);
}


sub new {
    return new_private(@_);
}


sub type {
    my ($self) = @_;

    return "ENUM";
}


sub value {
    my ($self) = @_;

    return @$self;
}


sub add {
    my ($self, $new_value) = @_;

    push(@$self, $new_value);
}


sub length {
    my ($self) = @_;

    return $#$self + 1;
}


sub iter {
    my ($self, $f) = @_;

    foreach my $v (@$self) {
	&$f($v);
    };
}


sub ocaml_repr {
    # Return the representation that the OCaml stubs can understand.
    return [ 1, shift @_];
}


sub this_object_is_an_UI_variable {
}


# TODO: more methods

######################################################################

package UI::Variable::DynEnum;
use strict;

# Representation:
# List of references to pairs (internal_value, external_value).
# The object is a blessed reference to this list.

sub new_private {
    my ($class, @pairs) = @_;

    my $v = \@pairs;

    return bless($v, $class);
}


sub new {
    return new_private(@_);
}


sub type {
    my ($self) = @_;

    return "DYNENUM";
}


sub value {
    my ($self) = @_;

    return @$self;
}


sub add {
    my ($self, $new_internal_value, $new_external_value) = @_;

    push(@$self, [ $new_internal_value, $new_external_value] );
}


sub length {
    my ($self) = @_;

    return $#$self + 1;
}


sub iter {
    my ($self, $f) = @_;

    foreach my $v (@$self) {
	my ($i,$e) = @$v;
	&$f($i,$e);
    };
}


sub assoc {
    my ($self, $key) = @_;

    foreach my $pair (@$self) {
	my ($internal, $external) = @$pair;
	if ($internal eq $key) {
	    return $external;
	};
    };
    return undef;
}


sub ocaml_repr {
    # Return the representation that the OCaml stubs can understand.
    return [2, shift @_];
}


sub this_object_is_an_UI_variable {
}


# TODO: more methods

######################################################################

package UI::Variable::Dialog;
use strict;

# Representation:
# Reference to object reference of class UI::Object, or
# Reference to undef.

sub new_private {
    my ($class, $obj) = @_;

    if (defined($obj)) {
	if (!UNIVERSAL::can($obj, 'this_object_is_an_UI_object')) {
	    die "UI::Variable::Object::new: This is not an UI object";
	};

	if (!defined($obj->{'id'})) {
	    die "UI::Variable::Object::new: Cannot fill object variable with unregistered object";
	};
    };

    my $v = \$obj;

    return bless($v, $class);
}


sub new {
    return new_private(@_);
}


sub type {
    my ($self) = @_;

    return "OBJECT";
}


sub value {
    my ($self) = @_;

    return $$self;
}


sub ocaml_repr {
    # Return the representation that the OCaml stubs can understand.
    my ($self) = @_;
    if (defined($$self)) {
	my $obj = $$self;
	return [3, $obj->{'id'} ];
    };
    return [3, undef];
}


sub this_object_is_an_UI_variable {
}

package UI::Variable::Object;                # The old name of UI::Variable::Dialog
use vars qw(@ISA);
@ISA = qw(UI::Variable::Dialog);


######################################################################

package UI::Variable::Alist;
use strict;

# Representation:
# List of references to pairs (key_value, variable).
# The object is a blessed reference to this list.

sub new {
    my ($class, @pairs) = @_;

    my $v = \@pairs;

    return bless($v, $class);
}


sub new_private {
    my ($class, @raw_pairs) = @_;

    my @pairs;
    foreach my $p (@raw_pairs) {
	my ($k, $raw_value) = @$p;
	my @v = @$raw_value;
	my $inner_class = shift @v;
	my $x = $inner_class->new_private(@v);
	push(@pairs, [$k, $x] );
    };
    my $v = \@pairs;

    return bless($v, $class);
}


sub type {
    my ($self) = @_;

    return "ALIST";
}


sub value {
    my ($self) = @_;

    return @$self;
}


sub add {
    my ($self, $new_internal_value, $new_external_value) = @_;

    push(@$self, [ $new_internal_value, $new_external_value] );
}


sub length {
    my ($self) = @_;

    return $#$self + 1;
}


sub iter {
    my ($self, $f) = @_;

    foreach my $v (@$self) {
	my ($i,$e) = @$v;
	&$f($i,$e);
    };
}


sub assoc {
    my ($self, $key) = @_;

    foreach my $pair (@$self) {
	my ($k, $var) = @$pair;
	if ($k eq $key) {
	    return $var;
	};
    };
    return undef;
}


sub ocaml_repr {
    # Return the representation that the OCaml stubs can understand.
    my ($self) = @_;

    my @r = ();

    foreach my $pair (@$self) {
	my ($k, $var) = @$pair;
	push(@r, [ $k, $var->ocaml_repr() ] );
    };

    return [4, \@r];
}


sub this_object_is_an_UI_variable {
}


# TODO: more methods

######################################################################

1;

=head1 NAME

UI::Variable - Value containers for instance variables of uiobjects

=head1 SYNOPSIS

    use UI::Variable;

=head2 String variables

    my $var = UI::Variable::String->new("string value");

    my $s = $var->value();     # return the string value in $var

=head2 Enumerator variables

    my $var = UI::Variable::Enum->new("a", "b", "c");
    # Creates the enumerator value with internal values a, b, c.

    my @e = $var->value();    # get the list of internal values

    $var->add("d");           # add another value to the list

    my $l = $var->length();   # return the number of values

    $var->iter( sub {         # Iterate over internal values $x
	my ($x) = @_;
	...
    });

=head2 Dynamic enumerator variables

    my $var = UI::Variable::DynEnum->new([ "a", "The letter A" ],
					 [ "b", "The letter B" ],
					 [ "c", "The letter C" ]);
    # Creates the enumerator value with internal values a, b, c and
    # corresponding external values

    my @e = $var->value();               # get the list of pairs

    $var->add("d", "The letter D");      # add another pair to the list

    my $l = $var->length();              # return the number of pairs

    $var->iter( sub {                    # Iterate over pairs
	my ($x_int, $x_ext) = @_;
	...
    });

    my $x_ext = $var->assoc("c");
    # associate the ext value to an int value

=head2 Associative variables

    my $var = UI::Variable::Alist->new([ "x", $var_x ],
				       [ "y", $var_y ],
				       [ "z", $var_z ]);
    # Creates the associative value with keys "x", "y", "z"
    # corresponding variable values

    my @e = $var->value();               # get the list of pairs

    $var->add("w", $var_w);              # add another pair to the list

    my $l = $var->length();              # return the number of pairs

    $var->iter( sub {                    # Iterate over pairs
	my ($x_key, $x_var) = @_;
	...
    });

    my $x_var = $var->assoc("y");
    # associate the variable value to a key

=head2 Dialog variables

    my $var = UI::Variable::Dialog->new( $obj );
    # Encapsulates the UI::Dialog $obj as variable value

    my $obj = $var->value();           # Get the object back

=head1 DESCRIPTION

The package UI::Variable defines a number of Perl classes for the structured
values used by WDialog:

=over 4

=item * UI::Variable::String - String values

=item * UI::Variable::Enum - Declared enumerators

=item * UI::Variable::DynEnum - Dynamic enumerators

=item * UI::Variable::Dialog - Dialogs as variable values

=item * UI::Variable::Alist - Associative variables

=back

These classes wrap up the corresponding Caml values and make them accessible
from Perl.

=head1 GETTING AND SETTING VARIABLES

The class C<UI::Dialog> has the methods to get and set variables:

  $var = $dlg->variable("name");

This method returns the value of the C<UI::Dialog> $dlg as one of the
C<UI::Variable> objects $var, depending on the type of the variable one
of the classes is selected.

This method sets the value of the variable:

  $dlg->set_variable("name", $var);

Note that C<UI::Dialog> has special methods for strings and enumerators,
so it is not always necessary to use the classes defined here.

=head1 UI::Variable::String

=over 4

=item * $var = UI::Variable::String->new("value")

Creates a new string object that can be put into a string variable.

=item * $s = $var->value()

If $var is a UI::Variable::String object, the string value will be returned.

=back


=head1 UI::Variable::Enum

=over 4

=item * $var = UI::Variable::Enum->new("item1", "item2", ...)

Creates a new enumerator object that can be put into a variable typed as
declared enumerator. The elements of the array must be declared internal
values of the enumerator.

=item * @e = $var->value()

Returns the value of the enumerator. The array @e contains all selected
items of the enumerator.

=item * $var->add("newitem")

Adds the passed item to the set of selected items of the enumerator $var.

=item * $n = $var->length()

Returns the number of items.

=item * $var->iter($f)

Calls the function $f for every item of the enumerator. The function gets
as only argument the current item.

=back

=head1 UI::Variable::DynEnum

=over 4

=item * $var = UI::Variable::DynEnum->new(["item1", "dispitem1"], ...)

Creates a new enumerator object that can be put into a variable typed as
dynamic enumerator. The method gets an array of two-element arrays as
input. This first element is the internal value, and the second element
is the external value.

=item * @e = $var->value()

Returns the set of items of the dynamic enumerator value. The array @e is
an array of two-element arrays. The first value is the internal value,
and the second is the external value.

=item * $var->add("item", "dispitem")

Adds another item pair to the set.

=item * $n = $var->length()

Returns the number of item pairs of the set.

=item * $var->iter($f)

Calls the function $f for every item of the enumerator. The function gets
two arguments: The first is the internal value, and the second is the
external value.

=back

=head1 UI::Variable::Dialog

=over 4

=item * $var = UI::Variable::Dialog->new($dlg)

Wraps the C<UI::Dialog> $dlg into a container $var that can be put into a
dialog variable.

=item * $var = UI::Variable::Dialog->new(undef)

Creates a dialog container that does not contain anything.

=item * $dlg = $var->value()

Returns the dialog object that is contained in $var, or returns
undef.

=back

=head1 UI::Variable::Alist

=over 4

=item * $var = UI::Variable::Alist->new(["key1", $val1], ... )

Creates a new associative list from a number of 2-element arrays.
The first elements are the keys, and the second elements are the
corresponding values. The values must be object of the classes
C<UI::Variable::String>, C<UI::Variable::Enum>, C<UI::Variable::DynEnum>,
or C<UI::Variable::Dialog>, but not C<UI::Variable::Alist> (i.e. no
nested alists).

=item * @e = $var->value()

Returns the pairs if keys and values of the associative list. The array @e is
an array of two-element arrays. The first values are the keys,
and the second values are the object values.

=item * $var->add("key", $val)

Adds another key/value pair to the list.

=item * $n = $var->length()

Returns the number of key/value pairs of the list.

=item * $var->iter($f)

Calls the function $f for every key/value pair of the list. The function gets
two arguments: The first is the key, and the second is the object value.

=item * $val = $var->assoc("key")

Returns the object value that corresponds to the passed key, or returns
undef.

=back


=cut

# ======================================================================
# History:
#
# $Log: Variable.pm,v $
# Revision 3.3  2002-03-05 16:22:27  stolpmann
# 	Updated docs.
#
# Revision 3.2  2002/02/14 16:24:13  stolpmann
# 	Added copyright notice
#
# Revision 3.1  2002/02/12 23:11:25  stolpmann
# 	Initial revision at sourceforge
#
# Revision 1.3  2002/01/21 14:25:37  gerd
# 	Changed for upcoming WDialog 2
#
# Revision 1.2  2000/12/04 17:03:31  gerd
# 	Removed the irritating comments of Joachim. Added a pointer
# to the reference documentation.
#
# Revision 1.1  2000/04/13 17:42:55  gerd
# 	Initial revision.
#
#
