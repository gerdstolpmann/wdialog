# <COPYRIGHT>
# Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH,
#                Gerd Stolpmann
#
# <GPL>
# This file is part of WDialog.
#
# WDialog is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# WDialog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with WDialog; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# </>
#

# $Id: Template.pm,v 3.4 2002-03-05 16:22:27 stolpmann Exp $
# ----------------------------------------------------------------------
#

package UI::Template;

use UI::Stubs;
use UI::Dialog;
use Carp;
use strict;


# mostly class methods

sub get {
    my ($class, $name) = @_;
    my $id = UI::Stubs::template_get $name;
    return bless(\$id, $class);
}


sub apply {
    my ($class, $tmpl, $params) = @_;
    if (!UNIVERSAL::can($tmpl,
			'this_object_is_an_UI_template')) {
	croak "UI::Template::apply: Not a template";
    };
    my $dialog = $UI::Dialog::CURRENT_DIALOG;
    my $id = UI::Stubs::template_apply($dialog->{'id'}, $$tmpl, $params);
    return bless(\$id, $class);
}


sub concat {
    my ($class, $sep, $args) = @_;
    if (!UNIVERSAL::can($sep,
			'this_object_is_an_UI_template')) {
	croak "UI::Template::apply: Not a template";
    };
    foreach my $a (@$args) {
	if (!UNIVERSAL::can($a,
			    'this_object_is_an_UI_template')) {
	    croak "UI::Template::apply: Not a template";
	};
    };
    my $id = UI::Stubs::template_concat($$sep, $args);
    return bless(\$id, $class);
}


sub empty {
    my ($class) = @_;
    my $id = UI::Stubs::template_empty(0);
    return bless(\$id, $class);
}


sub text {
    my ($class, $s) = @_;
    my $id = UI::Stubs::template_text($s);
    return bless(\$id, $class);
}


sub html {
    my ($class, $s) = @_;
    my $id = UI::Stubs::template_html($s);
    return bless(\$id, $class);
}


# the only instance method

sub to_string {
    my ($self, $obj) = @_;
    if (!UNIVERSAL::can($obj,
			'this_object_is_an_UI_object')) {
	croak "UI::Template::to_string: Not a UI object";
    };
    my $obj_id = $obj->{'id'};
    return UI::Stubs::template_to_string($obj_id, $$self);
}


sub this_object_is_an_UI_template {
}


1;

=head1 NAME

UI::Template - Access to WDialog templates

=head1 SYNOPSIS

    use UI::Template;

    my $template = UI::Template->get("XML name of template");

    my $tree = UI::Template->apply($template,
				    { 'param1', 'value1', ... });

    my $tree = UI::Template->concat($tree_sep,
				    [ $tree1, $tree2, ...]);
    # Result is the concatenation of $tree1, $tree2 etc,
    # where $tree_sep separates the elements of the list

    my $tree = UI::Template->empty();
    # A tree expanding to the empty string

    my $tree = UI::Template->text("a & b => c");
    # A tree expanding to the passed string; the characters <,>,& etc are
    # transformed to &lt;, &gt;, &amp; etc.

    my $tree = UI::Template->html("<b>bold text</b>");
    # A tree expanding literally to the passed string.

    my $s = $tree->to_string($dlg);
    # Expand the $tree in the context of the UI::Dialog $dlg
    # and return the resulting string.

=head1 DESCRIPTION

This module provides access to the template system of WDialog.

=head1 METHODS

=over 4

=item * $template = UI::Template->get("XML name of template")

Returns a handle to the named template. This template can be used as
argument for C<apply>.

=item * $tree = UI::Template->apply($template, { 'param1', 'value1', ... })

Instantiates the template by substituting the C<from-caller> parameter
param1 with value1, etc. All parameters of the template must be substituted.
The function returns a handle to the resulting XML tree.

=item * $tree = UI::Template->concat($tree_sep, [ $tree1, $tree2, ...])

Concatenates the passed trees, and the separator is $tree_sep. Returns
a new tree.

=item * $tree = UI::Template->empty()

Returns an empty tree.

=item * $tree = UI::Template->text($s)

Returns a tree that expands to the string $s where the special HTML characters
<, >, etc are replaced by their corresponding entities &lt;, &gt; etc.

=item * $tree = UI::Template->html($s)

Returns a tree that expands exactly to the string $s, so you can put HTML
markup into $s.

=item * $s = $tree->to_string($dlg)

Expands the $tree to a string in the context of the C<UI::Dialog> $dlg.

=back

=cut


# ======================================================================
# History:
#
# $Log: Template.pm,v $
# Revision 3.4  2002-03-05 16:22:27  stolpmann
# 	Updated docs.
#
# Revision 3.3  2002/02/28 18:50:44  stolpmann
# 	Continued with perlapi (lots of bugfixes)
#
# Revision 3.2  2002/02/14 16:24:13  stolpmann
# 	Added copyright notice
#
# Revision 3.1  2002/02/12 23:11:25  stolpmann
# 	Initial revision at sourceforge
#
# Revision 1.2  2002/01/21 14:25:37  gerd
# 	Changed for upcoming WDialog 2
#
# Revision 1.1  2000/04/13 17:42:55  gerd
# 	Initial revision.
#
#
