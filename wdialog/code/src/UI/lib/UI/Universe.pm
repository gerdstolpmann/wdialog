# <COPYRIGHT>
# Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH,
#                Gerd Stolpmann
#
# <GPL>
# This file is part of WDialog.
#
# WDialog is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# WDialog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with WDialog; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# </>
#

# $Id: Universe.pm,v 3.3 2002-03-05 16:22:27 stolpmann Exp $
# ----------------------------------------------------------------------
#

package UI::Universe;

use UI::Stubs;
use UI::Dialog;
use strict;

sub get {
    my ($name) = @_;
    return UI::Stubs::universe_create($name);
}

sub create {
    my ($name) = @_;
    return UI::Stubs::universe_create($name);
}

=head1 NAME

UI::Universe - Registry of the classes implementing the dialogs

=head1 SYNOPSIS

    use UI::Universe;

    my $dlg = UI::Universe::create("XML name of dialog");

    my $dlg = UI::Universe::get("XML name of dialog");
    # compatibility interface, same as create.

=head1 DESCRIPTION

The universe contains which dialog corresponds to which Perl class. After
the class has been registered (see UI::Dialog), it is possible to create
new dialog objects by invoking the C<create> method of the universe.
The argument of C<create> is the XML name of the dialog (as defined
by the ui:dialog XML element), the result value is a new object of the
corresponding Perl class.

=cut


1;

# ======================================================================
# History:
#
# $Log: Universe.pm,v $
# Revision 3.3  2002-03-05 16:22:27  stolpmann
# 	Updated docs.
#
# Revision 3.2  2002/02/14 16:24:13  stolpmann
# 	Added copyright notice
#
# Revision 3.1  2002/02/12 23:11:25  stolpmann
# 	Initial revision at sourceforge
#
# Revision 1.2  2002/01/21 14:25:37  gerd
# 	Changed for upcoming WDialog 2
#
# Revision 1.1  2000/04/13 17:42:55  gerd
# 	Initial revision.
#
#
