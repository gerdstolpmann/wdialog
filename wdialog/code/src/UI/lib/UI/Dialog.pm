# <COPYRIGHT>
# Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH,
#                Gerd Stolpmann
#
# <GPL>
# This file is part of WDialog.
#
# WDialog is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# WDialog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with WDialog; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# </>
#

# $Id: Dialog.pm,v 3.5 2002-03-04 18:50:36 stolpmann Exp $
# ----------------------------------------------------------------------
#

package UI::Dialog;

use UI::Stubs;
use UI::Variable;
use strict;


# backwards compatibility: (can be removed)
sub reset {
}


use vars qw($CURRENT_DIALOG);
# The object whose methods prepare_page or handle were called last.

sub register {
    # registers that the $class implements the UI object $name (i.e. $name
    # is the identifier from the XML file describing the UI object)
    my ($class, $name) = @_;

    # Create the closures that realize the callbacks "prepare_page" and
    # "handle":

    # prepare: The argument $s is the perl object
    my $prepare = sub { my ($s) = @_; $CURRENT_DIALOG = $s; $s->prepare_page; };

    # handle: The argument $s is the perl object. Return value is either
    # the ID of the next dialog object, or undef.
    my $handle  = sub { my ($s) = @_;
			$CURRENT_DIALOG = $s;
			my $r = $s->handle;
			return undef if !defined($r);
			if (UNIVERSAL::can($r,
					   'this_object_is_an_UI_object')) {
			    return $r->{'id'};
			} else {
			    die "UI::Object $class: 'handle' callback returned with illegal result";
			};
		    };

    # create: This closure is called when the universe creates a new
    # object for this dialog. The argument $id is the already allocated
    # object ID. The return value is the perl object.
    my $create = sub { my ($id) = @_;
		       my $dlg = bless({'id' => $id}, $class);
		       return $dlg;
		   };

    # Now register the callbacks for the dialog $name:

    UI::Stubs::universe_register($name, $prepare, $handle, $create);
}


sub this_object_is_an_UI_object {
    # This is a method that indicates that this is an UI object. It is used
    # for some checks.
}


sub name {
    my ($self) = @_;
    # Return the XML name of the UI object.

    my $id = $self->{'id'};
    return UI::Stubs::uiobject_name $id;
}


sub page_name {
    my ($self) = @_;
    # Return the name of the current page.

    my $id = $self->{'id'};
    return UI::Stubs::uiobject_page_name $id;
}


sub next_page {
    my ($self) = @_;
    # Return the name of the next page (if known).

    my $id = $self->{'id'};
    return UI::Stubs::uiobject_next_page $id;
}


sub set_next_page {
    my ($self, $pg) = @_;
    # Set the name of the next page.

    my $id = $self->{'id'};
    UI::Stubs::uiobject_set_next_page($id, $pg);
}


sub page_names {
    my ($self) = @_;
    # Return the list of all pages of this object.

    my $id = $self->{'id'};
    return @{UI::Stubs::uiobject_page_names $id};
}


sub variable {
    my ($self, $name) = @_;
    # Return the value of the variable $name as UI::Variable object.
    my $id = $self->{'id'};
    my @v = @{UI::Stubs::uiobject_variable($id,$name)};
    my $class = shift @v;
    return $class->new_private(@v);
}


sub string_variable {
    my ($self, $name) = @_;
    # Return the value of the string variable $name as string.
    my $id = $self->{'id'};
    return UI::Stubs::uiobject_string_variable($id,$name);
}


sub enum_variable {
    my ($self, $name) = @_;
    # Return the value of the enumerator variable $name as list of strings.
    my $var = $self->variable($name);
    die "not an enumerator variable" if ($var->type() ne 'ENUM');
    return $var->value;
}


sub set_variable {
    my ($self, $name, $value) = @_;
    # Set the variable $name to the $value passed as UI::Variable.
    if (!UNIVERSAL::can($value, 'this_object_is_an_UI_variable')) {
	die "UI::Object::set_variable must be called with an UI::Variable object";
    };
    my $id = $self->{'id'};
    my $r = $value->ocaml_repr();
    UI::Stubs::uiobject_set_variable($id,$name,$r);
}


sub set_string_variable {
    my ($self, $name, $val) = @_;
    # Set the variable $name to the $val passed as string.
    my $id = $self->{'id'};
    UI::Stubs::uiobject_set_string_variable($id,$name,$val);
}


sub unset_variable {
    my ($self, $name) = @_;
    # Reset the value of the variable $name to the default value.
    my $id = $self->{'id'};
    UI::Stubs::uiobject_unset_variable($id,$name);
}


sub event {
    my ($self) = @_;
    # Get the name of the current event.
    my $id = $self->{'id'};
    return @{UI::Stubs::uiobject_event($id)};
}


sub set_event {
    my ($self, @e) = @_;
    # Set the current event.
    my $id = $self->{'id'};
    UI::Stubs::uiobject_set_event($id, \@e);
}


sub init {
    my ($self, $name) = @_;
    # Re-initialize the object for page $name.
    my $id = $self->{'id'};
    UI::Stubs::uiobject_init($id,$name);
}


sub uploaded_file {
    my ($self, $name) = @_;
    my $id = $self->{'id'};
    my $fn = UI::Stubs::uiobject_upload_filename($id,$name);
    my $mt = UI::Stubs::uiobject_upload_mimetype($id,$name);
    my $sn = UI::Stubs::uiobject_upload_sysname($id, $name);
    return ($fn, $mt, $sn);
}

1;

=head1 NAME

  UI::Dialog - Dialog objects

=head1 SYNOPSIS

  # Derive from this class:

  use UI::Dialog;
  @ISA = qw(UI::Dialog);

  # Register a derived class:

  Class->register("XML object name");

  # Use an object $obj that is an UI::Object:

  my $xml_object_name    = $obj->name();

  my $current_page_name  = $obj->page_name();
  my $next_page_name     = $obj->next_page();
  my @names_of_all_pages = $obj->page_names();

  $obj->set_next_page("Name of next page");

  my $var = $obj->variable("variable name");
  # $var is an UI::Variable object.

  my $s = $obj->string_variable("variable name");
  # $s is an ordinary string

  my @e = $obj->enum_variable("variable name");
  # @e contains the internal names of the selected enumerator values

  $obj->set_variable("variable name", $var);
  # Here, $var must be an UI::Variable object

  $obj->set_string_variable("variable name", "string value");

  $obj->unset_variable("variable name");

  my @e = $obj->event();
  # Especially, $e[0] is the kind of event, and $e[1] is the name of the
  # event

  $obj->set_event('BUTTON', 'button name');

  $obj->init("page name");
  # Re-initialize the object, and display this page next.

  my ($filename, $mimetype, $sysname) = $obj->uploaded_file("upload-name");

=head1 DESCRIPTION

The class C<UI::Dialog> serves as a base class for dialog instances. The Perl
class C<UI::Dialog> corresponds to the Objective Caml class C<Wd_dialog.dialog>,
and has a very similar signature.

=head1 USE PATTERN

In order to define a new dialog class, create a new Perl class and inherit
from C<UI::Dialog>, as shown in the following example:

  package My_dialog;

  use UI::Dialog;
  @ISA = qw(UI::Dialog);

  sub prepare_page {
    my ($self) = @_;
    ...
  }

  sub handle {
    my ($self) = @_;
    ...
    return $result;
  }

You B<must> define the two methods C<prepare_page> and C<handle>, even if
these methods remain empty. Furthermore, note that C<handle> B<must> return
a sensible result value, otherwise you will get a runtime error when the
user clicks on a button.

The dialog class, here C<My_dialog>, should be registered in the universe
of known dialogs. To achieve this, execute

  My_dialog->register("my_dialog");

from the main program of the application. The passed string, "my_dialog" is
the XML name of the dialog that is used in the UI language, i.e. the name
occurring in <ui:dialog>.

=head1 INHERITED METHODS

The class C<UI::Dialog> defines a number of methods that are ready to be used
by the application programmer. Note that there is no constructor by default,
because WDialog has a built-in way of creating new dialog objects. See the
section below, "Creating new dialog objects", for a discussion.

=over 4

=item * Classname->register($xmlname)

This is a class method registering the class in the universe of known dialogs.
The passed string $xmlname is the name of the <ui:dialog> element that
corresponds to this class.

It is allowed that a Perl class registers several XML names.

=item * $dialog->name()

Returns the XML name of the dialog object $dialog.

=item * $dialog->page_name()

Returns the XML name of the current page of $dialog.

=item * $dialog->next_page()

Returns the XML name of the designated next page of $dialog. It is only reasonable
to call this method from the C<handle> callback, and the next page is already
initialized to the value of the C<goto> attribute of the button or link, if any.
The next page can be changed by calling C<set_next_page>.

=item * $dialog->set_next_page($name)

Sets the XML name of the next page that will be shown after the C<handle> callback
is over, and another page of the same dialog is displayed. Note that this value
will be ignored if it is switched to another dialog.

=item * $dialog->page_names()

Returns the XML names of all defined pages of this $dialog as array of strings.

=item * $dialog->variable($name)

Returns the variable $name of $dialog as C<UI::Variable> object. See
C<UI::Variable> for an overview of the possible varieties.

=item * $dialog->string_variable($name)

Returns the string variable $name of $dialog as string. It is an error if
the variable is not a string variable.

=item * $dialog->enum_variable($name)

Returns the declared enumerator variable $name of $dialog as array of items.
It is an error if the variable is not a string variable.

=item * $dialog->set_variable($name, $value)

Sets the string variable $name of $dialog to the passed $value.
It is an error if the variable is not a string variable.

=item * $dialog->unset_variable($name)

Resets the variable $name of $dialog to its initial value, i.e. the value
that is declared in the <ui:variable> element.

=item * $dialog->event()

Returns the current event - it is only reasonable to call this method from
C<handle>. The possible return values are described below in the section
about "Events".

=item * $dialog->set_event(@e)

Sets the current event. This is rarely a good idea. The method accepts the
same values that are returned by C<event>, and these are discussed below
in the section "Events".

=item * $dialog->init($page)

Re-initializes the $dialog for the passed $page (XML name). This can be used
in the C<prepare_page> callback to switch to a different page, but this is considered
as bad style. Re-initializing the dialog is an expensive operation.

=item * $dialog->uploaded_file($name)

Returns the uploaded file for the file upload widget $name (the name
used in <ui:file>). The method returns a triple ($filename, $content_type, $sysname).
$filename is the file name entered by the user into the file upload box.
$content_type is the MIME type as recognized by the web browser. $sysname is
the absolute file name where the file actually is. Note that the WDialog
engine will automatically delete the file $sysname after the request is over.
It is allowed to move the file away to another place without causing an
error condition.

Note that the triple is also returned when the user did not upload a file, or
when something went wrong. A good indication that there is an uploaded file
is that $filename is non-empty, and that $sysname denotes an existing file.

Note that this method works independently of the way the process is connected
with the web server.

=back



=head1 EVENTS

The method C<event> returns an array of two to five elements describing the
current event. The method C<set_event> accepts such an array as input. The
array can have one of the following layouts:

=over 4

=item * ("BUTTON", $name)

The event is that a button or a hyperlink called $name was pressed.

=item * ("INDEXED_BUTTON", $name, $index)

The event is that a button or a hyperlink called $name was pressed,
and that the button or link is indexed by $index.

=item * ("IMAGE_BUTTON", $name, $x, $y)

The event is that an image button $name was pressed at the coordinate
$x, $y.

=item * ("INDEXED_IMAGE_BUTTON", $name, $index, $x, $y)

The event is that an image button $name was pressed at the coordinate
$x, $y, and that the button is indexed by $index.

=item * ("NO_EVENT")

No (known) event could be recognized.

=item * ("POPUP_REQUEST", $param)

The event is that a popup window was requested with the parameter $param.

=back

Note that the type of the event is always at position 0 of the array,
and that the name can always be found at position 1.



=head1 USER-DEFINED METHODS

There are two methods that must be defined in the class inheriting from
C<UI::Dialog>: C<prepare_page>, and C<handle>. The method C<prepare_page>
is called just before the selected page of the dialog is displayed, and
its task is to set variables that are needed to show the page properly.
The method does not get any arguments, and it needs not to return any
result, it is only evaluated for its side effects.

The method C<handle> is called after the page has been displayed, and
after the user has generated an event (e.g. pressed a button). Its task
is to do any action that is should be done for the event. Furthermore,
the method determines what is to be displayed next: The current dialog?
Another dialog? Which page? - In the Perl version of C<handle> it is
possible to signal the next dialog/page as follows:

=over 4

=item * Display the same page again

The method returns C<undef>.

=item * Display another page of the same dialog:

First invoke $dialog->set_next_page($name), then return C<undef>.

=item * Display the start page of another dialog:

First get the Perl object of the other dialog, then return it.

=item * Display another page of another dialog:

First get the Perl object of the other dialog, then invoke
C<set_next_page>, and finally return the object.

=back

Any other result value but C<undef> or dialog objects is an error, and
causes that the program dies.


=head1 CREATING NEW DIALOG OBJECTS

Because WDialog automatically makes dialogs persistent (i.e. dialogs survive the
HTTP request/response cycle), it is normally the task of the WDialog core to create
the Perl objects realizing the dialog instances. However, sometimes it is necessary
to create a dialog object manually. Do this as follows: First, get an empty
dialog from C<UI::Universe>:

  my $dlg = UI::Universe::create($xmlname)

This creates a new Perl object of the registered class for $xmlname. In a
second step, you can initialize the dialog, e.g. by setting variables:

  $dlg->set_string_variable(...,...);
  $dlg->set_variable(...,...);

It is good style to put these statements into a constructor method:

  sub new {
    my ($class, $arg1, $arg2, ...) = @_;
    my $dlg = UI::Universe::create($xmlname);
    $dlg->set_string_variable(...,...);
    $dlg->set_variable(...,...);
    return $dlg
  }

Note that the passed $class is ignored; always the registered class will be
used for the Perl objects. Note also that only manually programmed invocations
of C<new> will trigger that the constructor is executed; when WDialog needs
to create the Perl object, it just calls C<UI::Universe::create> itself and
initializes the variables from the session state, but it does not call any
code supplied by the application programmer.

B<It is wrong> to call C<bless> in order to create the Perl object. The
Perl object is not an entity of its own, rather it is a proxy object for
an Objective Caml object, i.e. the method invocations for the Perl object
are forwarded to the Objective Caml object behind it. The C<UI::Universe>
knows this, and C<UI::Universe::create> creates both the Caml object and
the Perl proxy. In contrast to this, C<bless> would only create the Perl
object, but would not connect it to a resource that is understood by
WDialog.

=cut

# ======================================================================
# History:
#
# $Log: Dialog.pm,v $
# Revision 3.5  2002-03-04 18:50:36  stolpmann
# 	Updated docs.
#
# Revision 3.4  2002/02/28 18:50:44  stolpmann
# 	Continued with perlapi (lots of bugfixes)
#
# Revision 3.3  2002/02/14 16:24:13  stolpmann
# 	Added copyright notice
#
# Revision 3.2  2002/02/13 18:47:07  stolpmann
# 	Fixed memory management problem for perlapi.
#
# Revision 3.1  2002/02/12 23:11:25  stolpmann
# 	Initial revision at sourceforge
#
# Revision 1.1  2002/01/21 14:25:37  gerd
# 	Changed for upcoming WDialog 2
#
#
# ========================== OLD LOGS ==================================
# Revision 1.3  2000/12/06 17:53:38  gerd
# 	Updated.
#
# Revision 1.2  2000/04/17 10:12:57  gerd
# 	Improved file upload.
# 	Furthermore, the traditional CGI.pm package can be used,
# if the user prefers this.
#
# Revision 1.1  2000/04/13 17:42:55  gerd
# 	Initial revision.
#
#
