# <COPYRIGHT>
# Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH,
#                Gerd Stolpmann
#
# <GPL>
# This file is part of WDialog.
#
# WDialog is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# WDialog is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with WDialog; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# </>
#

# $Id: Object.pm,v 3.2 2002-02-14 16:24:13 stolpmann Exp $
# ----------------------------------------------------------------------
#

package UI::Object;

use UI::Dialog;

@ISA = qw(UI::Dialog);
# UI::DIalog is the new name of UI::Object

sub this_object_is_an_UI_object {
    # This is a method that indicates that this is an UI object. It is used
    # for some checks.
}

1;

# ======================================================================

=head1 NAME

  UI::Object - Compatibility module

=head1 SYNOPSIS

See UI::Dialog

=head1 DESCRIPTION

UI::Object is the old name of the UI::Dialog module.

=cut

# ======================================================================
# History:
#
# $Log: Object.pm,v $
# Revision 3.2  2002-02-14 16:24:13  stolpmann
# 	Added copyright notice
#
# Revision 3.1  2002/02/12 23:11:25  stolpmann
# 	Initial revision at sourceforge
#
# Revision 1.4  2002/01/21 14:25:37  gerd
# 	Changed for upcoming WDialog 2
#
# Revision 1.3  2000/12/06 17:53:38  gerd
# 	Updated.
#
# Revision 1.2  2000/04/17 10:12:57  gerd
# 	Improved file upload.
# 	Furthermore, the traditional CGI.pm package can be used,
# if the user prefers this.
#
# Revision 1.1  2000/04/13 17:42:55  gerd
# 	Initial revision.
#
#
