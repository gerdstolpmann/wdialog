/* <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH,
 *                Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 */

/* $Id: perlhelpers.h,v 3.2 2002-02-14 16:24:13 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 */

/* Perl includes: */

#define bool char

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

/* Caml includes */

#include "caml/mlvalues.h"



/* ---------------------------------------------------------------------- */
/* |                   C/ML conversion functions                        | */
/* ---------------------------------------------------------------------- */

void ml2c_perlvalues_scalar_value(value _v1, SV **_c2, void *ctx);
value c2ml_perlvalues_scalar_value(SV **_c2, void *ctx);
void ec_perlvalues_scalar_value(SV *sv);

void ml2c_perlvalues_array_value(value _v1, AV **_c2, void *ctx);
value c2ml_perlvalues_array_value(AV **_c2, void *ctx);
void ec_perlvalues_array_value(AV *av);

void ml2c_perlvalues_hash_value(value _v1, HV **_c2, void *ctx);
value c2ml_perlvalues_hash_value(HV **_c2, void *ctx);
void ec_perlvalues_hash_value(HV *av);

void ml2c_perlvalues_hash_entry(value _v1, HE **_c2, void *ctx);
value c2ml_perlvalues_hash_entry(HE **_c2, void *ctx);
void ec_perlvalues_hash_entry(HE *av);

/* ---------------------------------------------------------------------- */
/* |                   Perl macros as functions                         | */
/* ---------------------------------------------------------------------- */

int x_sviv(SV * x);
double x_svnv(SV * x);
void x_svpv(SV * x, char **s, int *l);
int x_svtrue(SV * x);
int x_sviok(SV * x);
int x_svnok(SV * x);
int x_svpok(SV * x);
int x_svok(SV * x);
SV *x_sv_undef(void);
SV *x_sv_true(void);
SV *x_sv_false(void);

int x_svrok(SV* x);

/* ---------------------------------------------------------------------- */
/* |                 Functions with additional casts                    | */
/* ---------------------------------------------------------------------- */


SV* newRV_inc_from_sv(SV * sv);
SV* newRV_inc_from_av(AV * av);
SV* newRV_inc_from_hv(HV * hv);

SV* newRV_noinc_from_sv(SV * sv);
SV* newRV_noinc_from_av(AV * av);
SV* newRV_noinc_from_hv(HV * hv);

SV* x_SvRV(SV*);
AV* x_AvRV(SV*);
HV* x_HvRV(SV*);

/* ---------------------------------------------------------------------- */
/* |                          Miscelleneous                             | */
/* ---------------------------------------------------------------------- */


int reftype(SV* x);

void pl_callback_noarg_noresult(SV *closure);
SV*  pl_callback_noarg_scalar(SV *closure);

void pl_callback_1arg_noresult(SV *closure, SV *arg);
SV*  pl_callback_1arg_scalar(SV *closure, SV *arg);


#ifndef CAMLIDL_TRANSIENT
#include "caml/camlidlruntime.h"
#endif

/* There seems to be a bug in camlidl-1.00: Under Unix, the symbol
 * IID_IUnkown is declared as external symbol, but never defined.
 * Fortunately, we do not use the COM stuff, so we need not to initialize
 * this variable.
 */

#ifndef _WIN32
IID IID_IUnknown;
#endif



/* ======================================================================
 * History:
 *
 * $Log: perlhelpers.h,v $
 * Revision 3.2  2002-02-14 16:24:13  stolpmann
 * 	Added copyright notice
 *
 * Revision 3.1  2002/02/12 20:41:37  stolpmann
 * 	Initial revision at sourceforge
 *
 * Revision 1.3  2002/01/21 14:27:50  gerd
 * 	Updated for upcoming Wdialog 2.
 *
 * Revision 1.2  2002/01/14 16:17:54  gerd
 * 	Update to ocaml-3.04
 *
 * Revision 1.1  2000/02/19 15:04:17  gerd
 * 	Initial revision.
 *
 *
 */
