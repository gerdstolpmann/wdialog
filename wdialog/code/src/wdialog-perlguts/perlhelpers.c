/* <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH,
 *                Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 */

/* $Id: perlhelpers.c,v 3.3 2002-02-14 16:24:13 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 */

#include "perlhelpers.h"
#include "caml/fail.h"

/* Required for perl-5.004 */

#include "patchlevel.h"

#if SUBVERSION == 4
#define PL_sv_undef sv_undef
#define PL_sv_yes   sv_yes
#define PL_sv_no    sv_no
#endif

#include <stdio.h>
/* debugging! */

/* ---------------------------------------------------------------------- */
/* |                   C/ML conversion functions                        | */
/* ---------------------------------------------------------------------- */


void ml2c_perlvalues_scalar_value(value _v1, SV **_c2, void *ctx)
{
*_c2 = *((SV * *) Bp_val(_v1));
}


value c2ml_perlvalues_scalar_value(SV **_c2, void *ctx)
{
value _v1;
_v1 = camlidl_alloc((sizeof(SV *) + sizeof(value) - 1) / sizeof(value), Abstract_tag);
*((SV * *) Bp_val(_v1)) = *_c2;
return _v1;
}


void ec_perlvalues_scalar_value(SV *sv) {
    if (sv == NULL) {
	raise_not_found();
    }
}


void ml2c_perlvalues_array_value(value _v1, AV **_c2, void *ctx)
{
*_c2 = *((AV * *) Bp_val(_v1));
}


value c2ml_perlvalues_array_value(AV **_c2, void *ctx)
{
value _v1;
_v1 = camlidl_alloc((sizeof(AV *) + sizeof(value) - 1) / sizeof(value), Abstract_tag);
*((AV * *) Bp_val(_v1)) = *_c2;
return _v1;
}


void ec_perlvalues_array_value(AV *av) {
    if (av == NULL) {
	raise_not_found();
    }
}

void ml2c_perlvalues_hash_value(value _v1, HV **_c2, void *ctx)
{
*_c2 = *((HV * *) Bp_val(_v1));
}


value c2ml_perlvalues_hash_value(HV **_c2, void *ctx)
{
value _v1;
_v1 = camlidl_alloc((sizeof(HV *) + sizeof(value) - 1) / sizeof(value), Abstract_tag);
*((HV * *) Bp_val(_v1)) = *_c2;
return _v1;
}


void ec_perlvalues_hash_value(HV *av) {
    if (av == NULL) {
	raise_not_found();
    }
}


void ml2c_perlvalues_hash_entry(value _v1, HE **_c2, void *ctx)
{
*_c2 = *((HE * *) Bp_val(_v1));
}


value c2ml_perlvalues_hash_entry(HE **_c2, void *ctx)
{
value _v1;
_v1 = camlidl_alloc((sizeof(HE *) + sizeof(value) - 1) / sizeof(value), Abstract_tag);
*((HE * *) Bp_val(_v1)) = *_c2;
return _v1;
}


void ec_perlvalues_hash_entry(HE *av) {
    if (av == NULL) {
	raise_not_found();
    }
}


/* ---------------------------------------------------------------------- */
/* |                   Perl macros as functions                         | */
/* ---------------------------------------------------------------------- */

int x_sviv(SV * x) {
    return SvIV(x);
}


double x_svnv(SV * x) {
    return SvIV(x);
}


void x_svpv(SV * x, char **s, int *l) {
    *s = SvPV(x, *l);
}


int x_svtrue(SV * x) {
    return SvTRUE(x);
}


int x_sviok(SV * x) {
    return SvIOK(x);
}


int x_svnok(SV * x) {
    return SvNOK(x);
}


int x_svpok(SV * x) {
    return SvPOK(x);
}


int x_svok(SV * x) {
    return SvOK(x);
}


SV *x_sv_undef(void) {
    return &PL_sv_undef;
}


SV *x_sv_true(void) {
    return &PL_sv_yes;
}


SV *x_sv_false(void) {
    return &PL_sv_no;
}


int x_svrok(SV *x) {
    return SvROK(x);
}

/* ---------------------------------------------------------------------- */
/* |                 Functions with additional casts                    | */
/* ---------------------------------------------------------------------- */

SV* newRV_inc_from_sv(SV * sv) {
    return newRV_inc((SV *) sv);
}

SV* newRV_inc_from_av(AV * av) {
    return newRV_inc((SV *) av);
}

SV* newRV_inc_from_hv(HV * hv) {
    return newRV_inc((SV *) hv);
}

SV* newRV_noinc_from_sv(SV * sv) {
    return newRV_noinc((SV *) sv);
}

SV* newRV_noinc_from_av(AV * av) {
    return newRV_noinc((SV *) av);
}

SV* newRV_noinc_from_hv(HV * hv) {
    return newRV_noinc((SV *) hv);
}

SV* x_SvRV(SV* x) {
    return (SV *) (SvRV(x));
}

AV* x_AvRV(SV* x){
    return (AV *) (SvRV(x));
}

HV* x_HvRV(SV* x){
    return (HV *) (SvRV(x));
}

/* ---------------------------------------------------------------------- */
/* |                          Miscelleneous                             | */
/* ---------------------------------------------------------------------- */

int reftype(SV* x) {
    switch (SvTYPE(SvRV(x))) {
    case SVt_IV:
	return 0;
    case SVt_NV:
	return 1;
    case SVt_PV:
	return 2;
    case SVt_RV:
	return 3;
    case SVt_PVAV:
	return 4;
    case SVt_PVHV:
	return 5;
    case SVt_PVCV:
	return 6;
    case SVt_PVGV:
	return 7;
    case SVt_PVMG:
	return 8;
    };
    return (-1);
}


void pl_callback_noarg_noresult(SV *closure) {
    dSP ;
    PUSHMARK(SP) ;
    perl_call_sv(closure, G_DISCARD|G_NOARGS) ;
}


SV*  pl_callback_noarg_scalar(SV *closure) {
    int count;
    SV *result, *sresult;
    dSP ;
    ENTER ;
    SAVETMPS ;
    PUSHMARK(SP) ;
    count = perl_call_sv(closure, G_NOARGS) ;
    SPAGAIN;
    if (count != 1) {
	failwith("pl_callback_noarg_scalar: Perl closure returned more than one result");
    };
    sresult = POPs;   /* this seems to be a mortal value */
    result = newSVsv(sresult);
    PUTBACK ;
    FREETMPS ;
    LEAVE ;
    /*
    if (SvREFCNT(sresult) != 0) {
	fprintf(stderr, "Memory leak!\n");
    };
    */
    return result;
}

void pl_callback_1arg_noresult(SV *closure, SV *arg) {
    dSP ;
    ENTER ;
    SAVETMPS ;
    PUSHMARK(SP) ;
    XPUSHs(arg);
    PUTBACK;
    perl_call_sv(closure, G_DISCARD) ;
    FREETMPS ;
    LEAVE ;
}


SV*  pl_callback_1arg_scalar(SV *closure, SV *arg) {
    int count;
    SV  *result, *sresult;
    dSP ;
    ENTER ;
    SAVETMPS ;
    PUSHMARK(SP) ;
    XPUSHs(arg);
    PUTBACK ;
    count = perl_call_sv(closure, 0) ;
    SPAGAIN;

    if (count != 1) {
	failwith("pl_callback_noarg_scalar: Perl closure returned more than one result");
    };
    sresult = POPs;   /* this seems to be a mortal value */
    result = newSVsv(sresult);
    PUTBACK ;
    FREETMPS ;
    LEAVE ;
/*
    if (SvREFCNT(sresult) != 0) {
	fprintf(stderr, "Memory leak!\n");
    };
*/
    return result;
}

/* ======================================================================
 * History:
 *
 * $Log: perlhelpers.c,v $
 * Revision 3.3  2002-02-14 16:24:13  stolpmann
 * 	Added copyright notice
 *
 * Revision 3.2  2002/02/13 18:47:07  stolpmann
 * 	Fixed memory management problem for perlapi.
 *
 * Revision 3.1  2002/02/12 20:41:35  stolpmann
 * 	Initial revision at sourceforge
 *
 * Revision 1.2  2002/01/21 14:27:50  gerd
 * 	Updated for upcoming Wdialog 2.
 *
 * Revision 1.1  2000/02/19 15:04:17  gerd
 * 	Initial revision.
 *
 *
 */
