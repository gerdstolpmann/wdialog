(*
 * <COPYRIGHT>
 * Copyright 2006 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_util.ml,v 3.2 2006-03-08 16:05:02 stolpmann Exp $ *)


let duplicates name_list =
  let rec loop found_names dup_names name_list =
    match name_list with
      | [] -> dup_names
      | name :: name_list' ->
	  if Wd_dictionary.mem name found_names then
	    let dup_names' =
	      Wd_dictionary.add name () dup_names in
	    loop found_names dup_names' name_list'
	  else
	    let found_names' =
	      Wd_dictionary.add name () found_names in
	    loop found_names' dup_names name_list'
  in
  let dict =
    loop Wd_dictionary.empty Wd_dictionary.empty name_list  in
  Wd_dictionary.keys dict
;;
	    

let split_re = Pcre.regexp "[ \t\r\n]+";;

let split s = Netstring_pcre.split split_re s;;

