(*
 * <COPYRIGHT>
 * Copyright 2006 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_catalog.ml,v 3.1 2006-03-08 16:05:02 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types
open Wd_util
open Pxp_types

type parsed_catalog =
    { pc_uplinks : string list;
      pc_messages : (string * string * string) list
    }


let parse_catalog ev_list =

  let rec parse_messages ev_stream =
    match ev_stream with parser 
      | [< 'E_start_tag("ui:msg",atts,_,_);
	   'E_end_tag("ui:msg",_);
	   msgs = parse_messages;
	>] ->
	  let lang = 
	    try List.assoc "lang" atts 
	    with Not_found -> 
	      raise(Formal_user_error("Attribute 'lang' required for <ui:msg>")) in
	  let internal = 
	    try List.assoc "internal" atts 
	    with Not_found -> 
	      raise(Formal_user_error("Attribute 'internal' required for <ui:msg>")) in
	  let external_ = 
	    try List.assoc "external" atts 
	    with Not_found -> internal in

	  (lang, internal, external_) :: msgs
	  
      | [< >] ->
	  []
  in

  try
    ( let ev_stream = Stream.of_list ev_list in
      match ev_stream with parser
	| [< 'E_start_tag("ui:catalog",atts,_,_);
	     msgs = parse_messages;
	     'E_end_tag("ui:catalog",_);
	     'E_end_of_stream
	  >] ->
	    let uplinks =
	      try split(List.assoc "uplink" atts)
	      with Not_found -> [] in
	    { pc_messages = msgs;
	      pc_uplinks = uplinks
	    }
    )
  with
    | Stream.Failure
    | Stream.Error "" ->
        raise(Formal_user_error "Syntax error in catalog")
    | Stream.Error msg ->
        raise(Formal_user_error("Syntax error in catalog: " ^ msg))
;;


class catalog ?(uplinks=[]) domain : extensible_message_catalog =
object(self)
  val mutable msgs = Wd_dictionary.empty

  method domain = domain
  method uplinks = uplinks

  method lookup ~language int =
    try
      let langcat = Wd_dictionary.find language msgs in
      Wd_dictionary.find int langcat
    with
      | Not_found -> 
	  raise (Not_found_in_catalog(domain, language, int))


  method as_table =
    assert false  (* TODO *)

  method add_message ~language int ext =
    let langcat =
      try Wd_dictionary.find language msgs 
      with Not_found -> Wd_dictionary.empty in

    let langcat' =
      Wd_dictionary.add int ext langcat in
    (* CHECK: duplicates? *)

    msgs <- Wd_dictionary.add language langcat' msgs

end
