(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_stdlib.mli,v 3.3 2002-02-16 17:29:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** An internal module containing the template stdlib *)

val stdlib_iso88591_1 : string
  (** The contents of stdlib.xml as Pxp_marshal'ed string (ISO-8859-1) *)

val stdlib_utf8_1 : string
  (** The contents of stdlib.xml as Pxp_marshal'ed string (UTF8) *)


(* ======================================================================
 * History:
 *
 * $Log: wd_stdlib.mli,v $
 * Revision 3.3  2002-02-16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:23  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 2.1  2002/02/07 17:29:37  gerd
 * 	Initial revision.
 *
 *
 *)
