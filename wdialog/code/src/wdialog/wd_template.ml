(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_template.ml,v 3.2 2002-02-14 16:15:21 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types
open Wd_transform
open Pxp_document
open Pxp_types

module D = Wd_dictionary

exception Template_not_found of string

type template = template_type

type tree = syntax_tree


let get (app:application_type) n =
  try
    app # template n
  with
      Not_found ->
	raise (Template_not_found n)

let apply dialog tt params =
  let params = D.of_alist params in
  Wd_transform.instantiate
    ~context:( dialog#declaration#default_context )
    ~params
    dialog
    tt


let apply_byname ?(localized=true) dialog n params =
  let t =
    try
      if not localized then raise (Template_not_found "");
      match dialog # declaration # language_variable with
	  None -> raise (Template_not_found "");
	| Some v ->
	    let lang = dialog # string_variable v in
	    get dialog#application (n ^ "#" ^ lang)
    with
	Template_not_found _ ->
	  get dialog#application n
  in
  apply dialog t params


let apply_lazily (dialog:dialog_type) n params =
  ignore(get dialog#application n);  (* only check whether template n exists *)
  Wd_transform.mk_use_param_node
    (dialog#application#dtd)
    n
    params
;;


let text app s =
  Wd_transform.mk_text_node
    app#dtd
    s
;;


let html app s =
  Wd_transform.mk_html_node
    app#dtd
    s
;;


let empty app = text app ""


let concat app sep l =
  Wd_transform.concat
    ~sep: sep
    app#dtd
    l

let to_string obj t =
  let buffer = Buffer.create 1024 in
  let outch = new Netchannels.output_buffer buffer in
  let _app_decl = obj#application in
  let _obj_decl = obj#declaration in
  let vars = { within_popup = false;
	       current_page = obj # page_name;
	       popup_env_initialized = false;
	       condition_code = false;
	       serialize_session = (fun () ->
				      raise(Runtime_error("Wd_template.to_string [deferred exception]: Cannot serialize session data")));
	     } in
    (* [vars] works only outside popup windows *)

  Wd_transform.to_html
    ~vars
    obj
    outch
    t;
  Buffer.contents buffer


(* ======================================================================
 * History:
 *
 * $Log: wd_template.ml,v $
 * Revision 3.2  2002-02-14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:20  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.13  2002/02/05 18:47:21  gerd
 * 	Minor follow-up change
 *
 * Revision 1.12  2002/01/31 23:06:39  gerd
 * 	Revised conditional expansion (ui:if, ui:ifvar, ui:iflang,
 * ui:cond).
 * 	Added some support for internationalization (xml:lang).
 *
 * Revision 1.11  2002/01/26 22:38:26  gerd
 * 	Changed the type for passing parameters to templates. It is
 * now syntax_tree dict instead of a simple list. Furthermore, the
 * method [instantiate] no longer returns the new context. The new element
 * ui:context creates the new contexts now.
 *
 * Revision 1.10  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.9  2000/12/06 15:28:20  gerd
 * 	to_string: (Still buggy) support for the ~self_url parameter.
 * 	html: Implemented for the first time
 *
 * Revision 1.8  2000/11/30 18:39:49  gerd
 * 	Changed for new interface of Parse.
 *
 * Revision 1.7  2000/09/25 13:22:13  gerd
 * 	New ui:popup element
 *
 * Revision 1.6  2000/09/21 15:12:34  gerd
 * 	Updated for O'Caml 3 and PXP
 *
 * Revision 1.5  2000/05/10 13:54:05  gerd
 * 	Bugfix.
 *
 * Revision 1.4  2000/05/08 15:30:38  gerd
 * 	The default context of the object is passed when a template
 * is instantiated.
 *
 * Revision 1.3  2000/05/08 10:35:26  gerd
 * 	When calling "to_html", an empty context is passed. (Perhaps
 * this should be changed to a "default context".)
 *
 * Revision 1.2  2000/04/17 10:10:52  gerd
 * 	New Cgi module.
 * 	File upload strongly improved.
 *
 * Revision 1.1  2000/04/13 17:42:58  gerd
 * 	Initial revision.
 *
 *
 *)
