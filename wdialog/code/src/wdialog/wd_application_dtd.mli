(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_application_dtd.mli,v 3.3 2002-02-16 17:29:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** Contains the DTD of the UI language *)

val dtd_1 : string  (** The WDialog DTD version 1 as string *)
val dtd_2 : string  (** The WDialog DTD version 2 as string *)

(* ======================================================================
 * History:
 * 
 * $Log: wd_application_dtd.mli,v $
 * Revision 3.3  2002-02-16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:22  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 2.1  2002/02/10 21:56:05  gerd
 * 	Initial revision.
 *
 * 
 *)
