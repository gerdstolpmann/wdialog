(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_ocamlversion.mli,v 1.1 2002-04-10 20:02:19 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

val revision_ocaml : string
  (* Returns a string that identifies the ocaml version *)

(* ======================================================================
 * History:
 * 
 * $Log: wd_ocamlversion.mli,v $
 * Revision 1.1  2002-04-10 20:02:19  stolpmann
 * 	Initial revision.
 *
 * 
 *)
