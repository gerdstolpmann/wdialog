(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_run_cgi.mli,v 3.6 2003-03-21 14:23:44 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** This module contains a customizable main program for CGIs *)

open Wd_types
open Netchannels

val adjust_gc : unit -> unit
  (** Adjust the garbage collector for short-living processes.
   * This is recommended if used in a CGI environment.
   *)

val run :
      ?charset:Pxp_types.rep_encoding ->      (* default: `Enc_iso88591 *)
      ?script:string ->                       (* default: use SCRIPT_NAME environment variable *)
      ?self_url:string ->                     (* default: derive from ~script *)
      ?uifile:string ->                       (* default: derive from ~script *)
      ?session_manager:session_manager_type -> (* defalt: instant_session_manager *)
      ?no_cache:bool ->                       (* default: true *)
      ?error_page:(out_obj_channel -> exn -> unit) ->  (* default: a simple error page *)
      ?cgi_config:Netcgi.config ->
      ?output_type:Netcgi.output_type ->
      ?response_header:response_header ->     (* default: see below *)
      ?reg:(universe_type -> unit) ->
      unit ->
	unit
  (** A customizable "main program" which processes CGI requests and generates
   * responses. It includes support for error handling.
   *
   * The simplest way to make a working CGI program is to
   * call [run ~reg ()]; the argument [~reg] registers the dialog classes.
   * The other arguments have reasonable defaults for a normal CGI
   * environment.
   *
   * The function passed to [~reg] gets the universe as argument. Its task
   * is to register the dialog classes by calling the method [register]
   * of the universe.
   *
   * [run] loads the UI file, initializes the CGI environment, processes
   * the request (see {!Wd_cycle}), and generates the response.
   * If an error happens, an error page is generated describing the
   * exception. The optional arguments modify the standard behaviour:
   *
   * @param charset determines the character set for the internal representation
   *   of strings and for the generated HTML pages. (Note that the parsed
   *   XML files can have character sets independent of this setting.)
   *   The default is [ISO-8859-1].
   *
   * @param script determines the file name of the invoked CGI script. 
   *   This parameter sets default for the other parameters [self_url]
   *   and [uifile] as follows: The
   *   file name of the ui file is derived from [~script] by changing the
   *   extension to ".ui" (or ".ui.bin" for compiled UI definitions).
   *   The default for [self_url] is derived from the basename of [script].
   *   -- If [script] is not passed, it is derived from the (CGI) environment
   *   variable [SCRIPT_NAME], and interpreted relative to the current
   *   working directory. 
   *   There is normally no reason to pass ~script.
   *   EXAMPLE: If your script is called "index.cgi", the UI definition
   *   file "index.ui" or "index.ui.bin" is used.
   *
   * @param self_url the URL which refers to the current script. By default,
   *   it is derived from [~script]. This value is put into the generated
   *   FORM elements as ACTION attribute.
   *
   * @param uifile is the path where the UI file can be found. By default,
   *   this argument is derived from [~script].
   *
   * @param no_cache If [true]: The CGI response contains header directives
   *   preventing the browser (and all caches between server and browser)
   *   from storing the response. This ensures that never a cache copy
   *   is taken, and for many browsers, the history is no longer usable
   *   (when hitting the "Back" button, the user only gets a message
   *   that the data is expired, but not the old page).
   *   The default is [true].
   *
   * @param error_page This function is called when an exception is caught.
   *   It can generate an error page by writing a nicely formatted
   *   message to the output channel. The default is a function that
   *   outputs a simple error page.
   *
   * @param cgi This argument is the CGI activation object to be used. This
   *   object represents the "outer world" for WDialog. The default is
   *   to use the class [std_activation] (of Ocamlnet) implementing the CGI/1.1
   *   standard.
   *
   * @param response_header If set, this parameter is taken as the initial
   *   HTTP response header while processing the request. If omitted, 
   *   the response header defaults to: The content type is text/html,
   *   the character set as determined by [charset], the caching header is
   *   set according to [no_cache].
   *)


(* ======================================================================
 * History:
 *
 * $Log: wd_run_cgi.mli,v $
 * Revision 3.6  2003-03-21 14:23:44  stolpmann
 * 	ocamldoc updated
 *
 * Revision 3.5  2003/01/15 23:04:47  stolpmann
 * 	New option ~uifile
 *
 * Revision 3.4  2003/01/04 21:55:25  stolpmann
 * 	new record response_header
 *
 * Revision 3.3  2002/02/16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:23  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.5  2002/02/05 18:43:55  gerd
 * 	It is now possible to pass the session_manager
 *
 * Revision 1.4  2002/01/30 15:14:03  gerd
 * 	New: ~charset.
 * 	Updated comments.
 *
 * Revision 1.3  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.2  2000/12/06 15:26:47  gerd
 * 	Support for the ~self_url argument.
 *
 * Revision 1.1  2000/11/30 18:29:07  gerd
 * 	Initial revision
 *
 *)
