(*
 * <COPYRIGHT>
 * Copyright 2005 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_brexpr_eval.mli,v 3.1 2006-03-08 00:56:45 stolpmann Exp $ *)

(** Evaluation of bracket expressions *)

open Wd_types
open Wd_brexpr


val apply_oe : application_type -> string -> string list -> string
  (** [apply_oe app s oelist]: Applies the output encodings listed in
    * [oelist] to [s] and returns the resulting string. The encoding
    * functions are taken from [app].
   *)

val eval_expr : 
       ?local_bindings:(string * var_value Lazy.t) list ->
       dialog_type -> expr -> var_value
  (** Evaluates a bracket expression in the scope of a dialog.
   * Raises [Eval_error_noloc] on evaluation error.
   *
   * The expression must not contain template parameters!
   * 
   * [local_bindings]: Variables are evaluated in the context of these
   * bindings which have higher precedence than global variables.
   *)

val eval_expr_oe : dialog_type -> expr_oe -> string
  (** Evaluates a bracket expression in the scope of a dialog,
   * takes the string result, and applies the output encodings.
   * Raises [Eval_error_noloc] on evaluation error.
   *
   * The expression must not contain template parameters!
   *)

val eval_string_expr : dialog_type -> expr_string -> string
  (** Evaluates a string containing bracket expression in the scope of a
    * dialog.
    * Raises [Eval_error] on evaluation error.
    *
    * The expression must not contain template parameters!
    *)

