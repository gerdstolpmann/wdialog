(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_dictionary.mli,v 3.5 2006-03-08 00:56:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)



(** Dictionaries are maps from string to any type. See the module [Map]
 * of the standard library for details.
 *)


type (+'a) t

val empty: 'a t

val add: string -> 'a -> 'a t -> 'a t

val find: string -> 'a t -> 'a

val remove: string -> 'a t -> 'a t

val mem: string -> 'a t -> bool

val iter: (string -> 'a -> unit) -> 'a t -> unit

val map: ('a -> 'b) -> 'a t -> 'b t

val mapi: (string -> 'a -> 'b) -> 'a t -> 'b t

val fold: (string -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

val of_alist : (string * 'a) list -> 'a t
  (** Convert the passed associative list to the corresponding dictionary *)

val to_alist : 'a t -> (string * 'a) list
  (** Convert the dictionary to an associative list *)

val keys : 'a t -> string list
  (** Return just the keys *)

val elements : 'a t -> 'a list
  (** Return only the elements *)

(* ======================================================================
 * History:
 * 
 * $Log: wd_dictionary.mli,v $
 * Revision 3.5  2006-03-08 00:56:45  stolpmann
 * Addition of Table_value, Matrix_value and Function_value.
 *
 * New parser for initial expressions. It is now possible to
 * use $[...] in variable initializers.
 *
 * Extended bracket expressions: functions, let expressions,
 * word literals, matrix literals.
 *
 * New type for message catalogs, although not yet implemented.
 *
 * Revision 3.4  2002/10/18 20:16:24  stolpmann
 * 	New: to_alist
 *
 * Revision 3.3  2002/02/16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:23  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 2.1  2002/01/26 22:39:36  gerd
 * 	Initial revision.
 *
 * 
 *)
