(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_templrep.mli,v 3.6 2006-03-08 00:56:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** Template representation *)

open Wd_types

type 'ext t
constraint 'ext = 'ext Pxp_document.node #Pxp_document.extension
  (** The type of prepared templates *)


type 'ext param =
    { param_tree : 'ext Pxp_document.node;     (** parameter as XML tree *)
      param_text : string Lazy.t;              (** parameter as string *)
    }
  (** The type of parameters to instantiate.
   * In [param_tree], the instantiation text must be passed as XML tree;
   * in [param_text], the same must be passed as string. The first
   * component is used if a parameter is found in element context;
   * the second component is used if a parameter is found in an attribute
   * value.
   * [param_text] is a lazy-evaluated value because it is unlikely that
   * the string transcription is actually used.
   *)


val prepare_tree_with_parameters :
      mk_uiencode:  (unit -> 'ext Pxp_document.node) ->
      string ->
      application_type ->
      'ext Pxp_document.node list ->
        'ext t
  (** [let pt = prepare_tree_with_parameters ~mk_uiencode name app nodes]:
   *
   * Prepares the node list [nodes] for the instantiation of parameters.
   * [nodes] remains unmodified; but references to inner nodes of [nodes]
   * are stored in [pt] (this should be transparent).
   * [app] is the application.
   * [name] is the name of the template; it is only used to generate
   * error messages.
   *
   * The preparation procedure scans all data nodes and all attribute
   * values for '$' parameters. [pt] contains a restructured copy of
   * [nodes] in which the parameters are specially marked up such that
   * the instantiation procedure can find them quickly.
   *
   * Recognized forms:
   * - [$name]:                       Replaced by the parameter [name] here
   * - [${name}]:                     Same
   * - [${name/enc1/enc2/...}]:       Apply the encodings [enc1],[enc2],... in turn before replacing
   * - [$[name]]:                     Replaced by the value of the dialog variable [name].
   * - [$[name/enc1/enc2/...]]:       Apply the encodings [enc1],[enc2],... in turn before replacing
   *
   * [~mk_uiencode]: This function must return an empty [<ui:encode/>] node. Such nodes are generated
   *    for [${name/enc1/enc2/...}] in element context.
   *
   *)

val get_parameters : 'ext t -> unit dict
  (** Returns the parameters that have been found in the template as dictionary *)

val instantiate :
                  dialog_type ->
                  'ext t ->
                  'ext param dict list ->
                  'ext Pxp_document.node ->
		  unit
  (** [instantiate dlg pt params container]:
   *
   * Instantiates the already prepared tree [pt], and sets the sub nodes
   * of [container] to the new instance (the instance is a node list).
   * The parameters are searched in [params], strictly from left to
   * right.
   *
   * An [Instantiation_error] is raised if something goes wrong.
   *)



(* ======================================================================
 * History:
 *
 * $Log: wd_templrep.mli,v $
 * Revision 3.6  2006-03-08 00:56:45  stolpmann
 * Addition of Table_value, Matrix_value and Function_value.
 *
 * New parser for initial expressions. It is now possible to
 * use $[...] in variable initializers.
 *
 * Extended bracket expressions: functions, let expressions,
 * word literals, matrix literals.
 *
 * New type for message catalogs, although not yet implemented.
 *
 * Revision 3.5  2003/03/21 14:23:44  stolpmann
 * 	ocamldoc updated
 *
 * Revision 3.4  2002/10/20 19:39:17  stolpmann
 * 	New feature: The brackets $[...] can contain expressions,
 * not only variables
 *
 * Revision 3.3  2002/02/16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:23  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.7  2002/01/30 15:13:33  gerd
 * 	New: The notations ${name/enc1/...} and $[name/enc1/...] are
 * recognized and handled properly.
 *
 * Revision 1.6  2002/01/27 19:13:15  gerd
 * 	New: [get_parameters]
 *
 * Revision 1.5  2002/01/26 22:38:26  gerd
 * 	Changed the type for passing parameters to templates. It is
 * now syntax_tree dict instead of a simple list. Furthermore, the
 * method [instantiate] no longer returns the new context. The new element
 * ui:context creates the new contexts now.
 *
 * Revision 1.4  2002/01/24 23:34:33  gerd
 * 	The function [instantiate] raises [Instantiation_error] on
 * errors.
 *
 * Revision 1.3  2001/05/21 12:52:27  gerd
 * 	Added a type constraint due to stricter type checking in
 * O'Caml 3.01
 *
 * Revision 1.2  2000/09/21 15:12:34  gerd
 * 	Updated for O'Caml 3 and PXP
 *
 * Revision 1.1  2000/05/08 10:36:37  gerd
 * 	Initial revision.
 *
 *
 *)
