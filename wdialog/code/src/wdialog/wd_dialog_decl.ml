(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_dialog_decl.ml,v 3.3 2002-10-18 20:18:46 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types

module H = Hashtbl
module D = Wd_dictionary

class dialog_decl : dialog_decl_type =
  object (self)

    val mutable name =
	    (None : string option)
    val mutable enumerations =
	    (H.create 10 : (string, enum_decl) H.t)
    val mutable variables =
	    (H.create 20 : (string, var_decl) H.t)
    val mutable variable_names =
	    []
    val mutable pages =
	    (H.create 10 : (string, 'page_type) H.t)
    val mutable page_names =
	    []
    val mutable start_page =
	    (None : string option)
    val mutable default_context =
	    (D.empty : 'page_type dict)
    val mutable language_variable =
	    (None : string option)

    (* ------------------------ Public --------------------------- *)

    method name =
      match name with
	  Some n -> n
	| None -> failwith "wd_dialog_decl # name"

    method enumeration enum_name =
      H.find enumerations enum_name

    method variable_names =
      variable_names

    method variable var_name =
      H.find variables var_name

    method page_names =
      page_names

    method page pg_name =
      H.find pages pg_name

    method start_page =
      match start_page with
	  Some n -> n
	| None -> failwith "wd_dialog_decl # start_page"

    method default_context = default_context

    method language_variable = language_variable

    method page_is_declared_as_popup name =
      let tree = self # page name in
      try
	tree # node # attribute "popup" = Pxp_types.Value "yes"
      with
	  Not_found -> true
	      (* DTD version 1: behave as if all pages were declared as popups *)

    (* -------------------- Restricted use only ------------------- *)

    method set_name n =
      name <- Some n

    method set_start_page n =
      start_page <- Some n

    method add_enumeration enum =
      if H.mem enumerations enum.enum_name then
	raise (Formal_user_error ("ui:enumeration `" ^ enum.enum_name ^
				  "' is declared twice"));
      H.add enumerations enum.enum_name enum;

    method add_variable var =
      (* because "." is now used for the dot notation: *)
      if String.contains var.var_name '.' then
	raise (Formal_user_error ("ui:variable `" ^ var.var_name ^
				  "': invalid name (contains dot)"));
      if H.mem variables var.var_name then
	raise (Formal_user_error ("ui:variable `" ^ var.var_name ^
				  "' is declared twice"));
      H.add variables var.var_name var;
      variable_names <- var.var_name :: variable_names;

    method add_page pg_name pg =
      if H.mem pages pg_name then
	raise (Formal_user_error ("ui:page `" ^ pg_name ^
				  "' is declared twice"));
      H.add pages pg_name pg;
      page_names <- pg_name :: page_names;

    method set_default_context context =
      default_context <- context

    method set_language_variable v =
      language_variable <- Some v

  end
;;

(* ======================================================================
 * History:
 *
 * $Log: wd_dialog_decl.ml,v $
 * Revision 3.3  2002-10-18 20:18:46  stolpmann
 * 	Because of the dot notation it is no longer allowed to declare
 * variables with a dot in their names
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:03  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.6  2002/02/05 18:45:37  gerd
 * 	New: method page_is_declared_as_popup.
 *
 * Revision 1.5  2002/01/31 23:06:39  gerd
 * 	Revised conditional expansion (ui:if, ui:ifvar, ui:iflang,
 * ui:cond).
 * 	Added some support for internationalization (xml:lang).
 *
 * Revision 1.4  2002/01/26 22:35:53  gerd
 * 	Using hashtables instead of associative lists
 *
 * Revision 1.3  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.2  2000/05/08 15:31:57  gerd
 * 	New slot storing the default context of the object.
 *
 * Revision 1.1  2000/04/13 17:42:58  gerd
 * 	Initial revision.
 *
 *
 *)
