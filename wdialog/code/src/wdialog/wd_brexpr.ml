(*
 * <COPYRIGHT>
 * Copyright 2005 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_brexpr.ml,v 3.1 2006-03-08 00:56:45 stolpmann Exp $ *)

open Wd_types

type fn_name =
    [ `Expr_fun_name of string
	(** A function name *)
    ]

type 'a expr_ap =
    [ `Expr_var of string
    | `Expr_strconst of string
    | `Expr_apply of ('a * 'a expr_ap list)
    | `Expr_param of (string * string list)
    | `Expr_matrix of (string * 'a expr_ap) list list
    | `Expr_fun of string list * 'a expr_ap
    | `Expr_let of (string * 'a expr_ap) list * 'a expr_ap
    ]

type expr_fn =
    [ fn_name | 'a expr_ap ] as 'a

type expr = expr_fn expr_ap

type expr_oe =
    [ `Expr_oe of expr * string list
    | expr
    ]

type expr_string = (expr_string_item * string) list

and expr_string_item =
   [ `Literal of string
   | expr_oe
   ]

(* Syntax:
 * oe_expr -> expr [ "/" oe_list ]
 * oe_list -> oe_name [ "/" oe_list ]
 * expr -> variable
 *       | number
 *       | "'" token
 *       | function "(" [arguments] ")" 
 *       | "fun" "(" formal_args ")." expr
 *       | "let" "(" binding ")." expr
 *       | "{" [ matrix ] "}"
 *       | "$" parameter-name
 *       | "${" parameter-name [ "/" oe_list ] "}"
 * function -> variable
 *       | "(" expr ")"
 * arguments -> expr [ "," arguments ]
 * formal_args -> variable [ "," formal_args ]
 * binding -> variable "=" expr [ "," binding ]
 * matrix -> row
 *         | row ";" matrix
 * row -> binding
 *)

module L = Wd_brexpr_lex

let parse_expr ~enable_param enc s : expr_oe =
  let enc = (enc : Pxp_types.rep_encoding :> Netconversion.encoding) in
  let scan = Wd_brexpr_lex.scan in
  let ulb = Netulex.ULB.from_string enc s in
  let buf = Netulex.Ulexing.from_ulb_lexbuf ulb in

  let raise_no_brace() =
    raise (Formal_user_error("Template parameters not allowed here")) 
  in

  let raise_bad() =
    raise (Formal_user_error("Bad syntax in bracket expression: " ^ s)) 
  in

  let rec parse tok =
    match tok with
      | L.Token "fun" ->
	  ( let next = scan buf in
	    match next with
	      | L.LParen ->
		  let args = parse_formal_args_1 (scan buf) in
		  let next' = scan buf in
		  let expr, next'' = parse next' in
		  (`Expr_fun(args,expr), next'')
	      | _ ->
		  raise_bad()
	  )
      | L.Token "let" ->
	  ( let next = scan buf in
	    match next with
	      | L.LParen ->
		  let blist = parse_binding_nonempty L.RParenDot in
		  let next' = scan buf in
		  let expr, next'' = parse next' in
		  (`Expr_let(blist,expr), next'')
	      | _ ->
		  raise_bad()
	  ) 
      | L.Token name ->
	  ( let next = scan buf in
	    match next with
	      | L.LParen ->
		  let args = parse_arguments() in
		  let next' = scan buf in
		  (`Expr_apply (`Expr_fun_name name, args), next')
		  
	      | _ ->
		  (`Expr_var name, next)
	  )
      | L.LParen ->
	  ( let next = scan buf in
	    let expr, next' = parse next in
	    if next' <> L.RParen then raise_bad();
	    let next'' = scan buf in
	    ( match next'' with
		| L.LParen ->
		    (* (fun-value)(arguments) *)
		    let args = parse_arguments() in
		    let next''' = scan buf in
		    (`Expr_apply( (expr :> expr_fn), args), next''')
		| _ ->
		    (* Simply additional parentheses *)
		    ( expr , next'')
	    )
	  )
      | L.Number n ->
	  (`Expr_strconst (string_of_int n), scan buf)
      | L.QToken name ->
	  (`Expr_strconst name, scan buf)
      | L.LBrace ->
	  ( let rows = parse_matrix() in
	    (`Expr_matrix rows, scan buf)
	  )
      | L.Dollar ->
	  if not enable_param then raise_no_brace();
	  ( let next = scan buf in
	    match next with
	      | L.Token name ->
		  (`Expr_param(name,[]), scan buf)
	      | L.LBrace ->
		  let next' = scan buf in
		  ( match next' with
		      | L.Token name ->
			  let next'' = scan buf in
			  let (oelist, next''') = 
			    ( match next'' with
				| L.Slash ->
				    parse_out_enc()
				| _ ->
				    ([], next'')
			    ) in
			  if next''' <> L.RBrace then
			    raise_bad();
			  (`Expr_param(name,oelist), scan buf)
		      | _ ->
			  raise_bad();
		  )
	      | _ ->
		  raise_bad()
	  )
      | _ ->
	  raise_bad()


  and parse_arguments () =
    let tok = scan buf in
    match tok with
      | L.RParen ->
	  []
      | _ ->
	  parse_arguments_1 tok

  and parse_arguments_1 tok =
    let (arg,next) = parse tok in
    match next with
      | L.RParen ->
	  [arg]
      | L.Comma ->
	  arg :: (parse_arguments_1 (scan buf))
      | _ ->
	  raise_bad()

(*
  and parse_formal_args () =
    let tok = scan buf in
    match tok with
      | L.RParenDot ->
	  []
      | _ ->
	  parse_formal_args_1 tok
 *)
  and parse_formal_args_1 tok =
    match tok with
      | L.Token name ->
	  let next = scan buf in
	  ( match next with
	      | L.RParenDot ->
		  [name]
	      | L.Comma ->
		  name :: (parse_formal_args_1 (scan buf))
	      | _ ->
		  raise_bad()
	  )
      | _ ->
	  raise_bad()

(*
  and parse_binding rsym =
    let tok = scan buf in
    if tok = rsym then
      []
    else
      let (b, tok') = parse_binding_1 tok in
      if tok' <> rsym then raise_bad();
      b
 *)
  and parse_binding_nonempty rsym =
    let tok = scan buf in
    let (b, tok') = parse_binding_1 tok in
    if tok' <> rsym then raise_bad();
    b

  and parse_binding_1 tok =
    match tok with
      | L.Token name ->
	  let tok' = scan buf in
	  if tok' <> L.Equal then raise_bad();
	  let tok'' = scan buf in
	  let (expr, tok''') = parse tok'' in
	  let b = (name, expr) in
	  ( match tok''' with
	      | L.Comma ->
		  let (br, tok'''') = parse_binding_1 (scan buf) in
		  (b :: br, tok'''')
	      | _ ->
		  ( [b], tok''')
	  )
      | _ ->
	  raise_bad()

  and parse_matrix () =
    let tok = scan buf in
    if tok = L.RBrace then
      []
    else
      parse_matrix_1 tok

  and parse_matrix_1 tok =
    let (b, tok) = parse_binding_1 tok in
    match tok with
      | L.Semicolon ->
	  let tok' = scan buf in
	  b :: parse_matrix_1 tok'
      | L.RBrace ->
	  [b]
      | _ ->
	  raise_bad()

  and parse_out_enc() =
    let tok = scan buf in
    match tok with
      | L.Token name ->
	  let tok' = scan buf in
	  ( match tok' with
	      | L.Slash ->
		  let r, tok'' = parse_out_enc() in
		  (name :: r, tok'')
	      | _ ->
		  ([name], tok')
	  )
      | _ ->
	  raise_bad()
  in
   
  let tok = scan buf in
  let expr, next = parse tok in
  ( match next with
      | L.Eof ->
	  (expr : expr :> expr_oe)
      | L.Slash ->
	  let oelist, next' = parse_out_enc() in
	  if next' <> L.Eof then raise_bad();
	  `Expr_oe (expr, oelist)
      | _ ->
	  raise_bad()
  )
;;


let param_re =
    (Pcre.regexp
       "\\$([a-zA-Z0-9_]+|{([^} \t\r\n]+)}|\\[([^\\]]+)\\]|\\$)")
        (* $name or ${name} or $[name] or $$ *)
;;


let param_slash_re = Pcre.regexp "/";;


let split_param s =
  (* s is either ${name/enc1/...} or $[name/enc1/...] *)
  let s = String.sub s 2 (String.length s - 3) in
  match Pcre.split ~rex:param_slash_re s with
      [] -> ("", [])              (* Will hopefully be later caught! *)
    | name :: enc -> (name, enc)
;;


let pcre_remove_groups l =
  (* Remove the silly Group and NoGroup markers *)
  List.filter
    (function (Pcre.Delim _|Pcre.Text _) -> true | _ -> false)
    l
;;


let parse_expr_string ~enable_param ~enable_brexpr enc s =
  let raise_no_brace() =
    raise (Formal_user_error("Template parameters not allowed here")) 
  in
  let raise_no_brexpr() =
    raise (Formal_user_error("Bracket expressions not allowed here")) 
  in
  let pcre_postprocess l =
    List.map
      (function
	   Pcre.Delim "$$" ->
	     (`Expr_strconst "$", "$$")
	 | Pcre.Delim p ->
	     if String.length p >= 2 then begin
	       if p.[1] = '{' then (
		 if not enable_param then raise_no_brace();
		 let name,enclist = split_param p in
		 (`Expr_param(name,enclist), p)
	       )
	       else
		 if p.[1] = '[' then (
		   if not enable_brexpr then raise_no_brexpr();
		   let e =
		     parse_expr 
		       ~enable_param
		       enc 
		       (String.sub p 2 (String.length p - 3)) in
		   ( (e : expr_oe :> expr_string_item) ,p)
		 )
		 else (
		   if not enable_param then raise_no_brace();
		   (`Expr_param (String.sub p 1 (String.length p - 1), []),p)
		 )
	     end
	     else (
	       if not enable_param then raise_no_brace();
	       (`Expr_param (String.sub p 1 (String.length p - 1), []), p)
	     )
	 | Pcre.Text t ->
	     (`Literal t, t)
	 | (Pcre.Group(_,_)|Pcre.NoGroup) ->
	     assert false
      )
      (pcre_remove_groups l)
  in
  pcre_postprocess
    (Pcre.full_split ~rex:param_re ~max:(-1) s)
;;


let params_in_expr_1 d (expr : expr_oe) =
  let rec collect d expr =
    match expr with
      | `Expr_oe(e, _) -> 
	  collect d (e :> expr_oe)
      | `Expr_apply(_, el) -> 
	  List.fold_left collect d (el :> expr_oe list)
      | `Expr_param(name,_) ->
	  Wd_dictionary.add name () d
      | `Expr_matrix nel ->
	  List.fold_left
	    (fun d row ->
	       List.fold_left 
		 (fun d (_,e) -> collect d (e :> expr_oe))
		 d 
		 row)
	    d
	    nel
      | `Expr_fun(_,e) ->
	  collect d (e :> expr_oe)
      | `Expr_let(nel, e) ->
	  collect 
	    (List.fold_left (fun d (_,e) -> collect d (e :> expr_oe)) d nel)
	    (e :> expr_oe)
      | `Expr_var _
      | `Expr_strconst _ ->
	  d
  in
  collect d expr
;;


let params_in_expr expr = 
  List.map
    fst
    (Wd_dictionary.to_alist
       (params_in_expr_1 Wd_dictionary.empty expr));;


let params_in_expr_string_1 d l =
  let rec collect d l =
    match l with
      | [] -> d
      | (`Literal _, _) :: l' -> collect d l'
      | (#expr_oe as e, _) :: l' -> collect (params_in_expr_1 d e) l'
  in
  collect d l
;;


let params_in_expr_string l =
  List.map
    fst
    (Wd_dictionary.to_alist
       (params_in_expr_string_1 Wd_dictionary.empty l));;


let subst_expr_params ~subst (expr : expr_oe) : expr_oe =
  let rec perform expr =
    match expr with
      | `Expr_param(p, l) ->
	  `Expr_strconst (subst (p, l))
      | `Expr_apply(f, l) ->
	  `Expr_apply(f, List.map perform l)
      | `Expr_matrix l ->
	  `Expr_matrix
	    (List.map 
	       (fun row ->
		  List.map
		    (fun (col_name, expr) -> (col_name, perform expr))
		    row)
	       l)
      | `Expr_fun (vars, e) ->
	  `Expr_fun(vars, perform e)
      | `Expr_let(l, e) ->
	  let l' =
	    List.map (fun (var, e') -> (var, perform e')) l in
	  `Expr_let (l', perform e)
      | any -> any
  and perform_oe (expr : expr_oe) =
    match expr with
      | `Expr_oe(e, oe_list) ->
	  `Expr_oe(perform e, oe_list)
      | (#expr as e) -> (perform e :> expr_oe)
  in
  perform_oe expr
;;



let subst_expr_string_params ~subst expr_str =
  List.map
    (fun (item, s) ->
       match item with
	 | (#expr_oe as e) ->
	     let e' =
	       subst_expr_params ~subst e in
	     ( (e' :> expr_string_item), s )
	 | _ -> (item, s)
    )
    expr_str
;;
