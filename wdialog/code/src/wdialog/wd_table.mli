(*
 * <COPYRIGHT>
 * Copyright 2005 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_table.mli,v 3.1 2006-03-08 00:56:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** This module defines functions for tables *)

open Wd_types

val empty_table : table
  (** Returns an empty table (no rows, no columns) *)

(*

val promote_enum : enum_decl -> string list -> table
val promote_dyn_enum : (string * string) list -> table
val promote_alist : (string * var_value) list -> table
val promote_matrix : head * string array array -> table
  (** Return tables accessing the values. The assumed head is
    * [ [| "int"; "ext" |] ] unless passed. For alists, only
    * strings can be represented; other values appear as empty
    * strings.
    *
    * This definition corresponds to the ui:iterate element.
   *)

val variable_as_table : dialog_type -> string -> table
  (** Accesses the named variable as table, promoted if necessary.
   * Fails with [Runtime_error] if promotion is not possible.
   *)

val elements : table -> string array list
  (** Returns all elements of the table by iterating completely over it *)

val compare : table -> table -> int
  (** Compares two tables by iterating over both *)

 *)
