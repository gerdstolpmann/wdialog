(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_dictionary.ml,v 3.4 2006-03-08 00:56:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)


module HS = struct (* Hashed strings *)

  type t = (string * int)    (* The string and its hash value *)

  let compare (a_s,a_h) (b_s,b_h) = 
    let d = a_h - b_h in
    if d = 0 then
      Stdlib.compare a_s b_s
    else
      d
end ;;


module HSM = Map.Make(HS) ;;

type (+'a) t = 'a HSM.t ;;

let empty = HSM.empty ;;

let add k x m =
  HSM.add (k, Hashtbl.hash k) x m ;;

let find k m =
  HSM.find (k, Hashtbl.hash k) m ;;

let remove k m =
  HSM.remove (k, Hashtbl.hash k) m ;;

let mem k m =
  HSM.mem (k, Hashtbl.hash k) m ;;

let iter f m =
  HSM.iter (fun (k,_) x -> f k x) m ;;

let map = HSM.map ;;

let mapi f m =
  HSM.mapi (fun (k,_) x -> f k x) m ;;

let fold f m y0 =
  HSM.fold (fun (k,_) x y -> f k x y) m y0 ;;

let rec of_alist l =
  match l with
      [] -> empty
    | (k,x) :: l' -> add k x (of_alist l') ;;


let to_alist m =
  fold
    (fun k x l -> (k,x)::l)
    m
    []
;;


let keys m =
  fold
    (fun k x l -> k::l)
    m
    []
;;


let elements m =
  fold
    (fun k x l -> x::l)
    m
    []
;;


(* ======================================================================
 * History:
 * 
 * $Log: wd_dictionary.ml,v $
 * Revision 3.4  2006-03-08 00:56:45  stolpmann
 * Addition of Table_value, Matrix_value and Function_value.
 *
 * New parser for initial expressions. It is now possible to
 * use $[...] in variable initializers.
 *
 * Extended bracket expressions: functions, let expressions,
 * word literals, matrix literals.
 *
 * New type for message catalogs, although not yet implemented.
 *
 * Revision 3.3  2002/10/18 20:16:24  stolpmann
 * 	New: to_alist
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:13  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 2.1  2002/01/26 22:39:36  gerd
 * 	Initial revision.
 *
 * 
 *)
