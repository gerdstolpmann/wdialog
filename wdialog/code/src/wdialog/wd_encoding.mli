(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_encoding.mli,v 3.6 2003-03-21 14:23:44 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)


(** This module contains the output encodings. The encodings are functions
 *  from [string] to [string]. They are usually entered into the 
 *  application by default (see the method [output_encoding] of the
 *  {!Wd_types.application_type}), and accessible by [<ui:encoding>] and
 *  other elements of the UI language.
 *
 *  The functions defined here assume that an ASCII-compatible character
 *  set is used.
 *)

val encode_as_html : string -> string
  (** Encodes strings as HTML:
   * - '<' becomes [&lt;]
   * - '>' becomes [&gt;]
   * - '"' becomes [&quot;]
   * - '&' becomes [&amp;]
   * 
   * All other characters remain unchanged.
   *
   * From [<ui:encoding>], this function is accessible under the name [html].
   *)

val encode_as_pre : string -> string
    (** Does the following encoding:
     * - ' ' becomes [&nbsp;]
     * - '\\n' becomes [<br>]
     * - '\\t' is expanded to a sequence of [&nbsp;], tab width is 8
     *
     * All other characters remain unchanged.
     *
     * From [<ui:encoding>], this function is accessible under the name [pre].
     *)

val encode_as_para : string -> string
  (** Multiple linefeeds are replaced by <p>. 
   *
   * From [<ui:encoding>], this function is accessible under the name [para].
   *)

val encode_as_js_string : string -> string
  (** Encodes strings such that they can be placed between quotes in
   * Javascript.
   * - '\\' becomes backslash backslash
   * - '"' becomes backslash double-quotes
   * - ''' becomes backslash single-quote
   * - '<' becomes \\x3c
   * - '%' becomes \\x25
   *
   * The characters 0 to 31 and 127 become \\xHH
   * All other characters remain unchanged.
   *
   * From [<ui:encoding>], this function is accessible under the name [js].
   *)

val encode_as_js_longstring : 
  enc:Pxp_types.rep_encoding -> string -> string
  (** Similar to [encode_as_js_string], but an additional rules prevents
   * that long lines result. (Javascript interpreters often cannot cope
   * with long lines.) The string is split up into pieces p1,...,pN
   * and these are connected by
   * p1" + \\n "p2" + \\n ... + \\n "pN
   *
   * From [<ui:encoding>], this function is accessible under the name [jslong].
   *
   * It is required to pass the character encoding of the strings as
   * argument [enc]. In the case the encoding uses multi-byte character
   * representations, it is avoided to break up these characters.
   *)

(** Note that you can combine the various encodings to get new effects.
 * Reasonable combinations are:
 * - encode_as_html then encode_as_pre
 * - encode_as_html then encode_as_para
 * - encode_as_html then encode_as_para then encode_as_pre
 * - encode_as_html then encode_as_js_string
 * - encode_as_html then encode_as_js_longstring
 *
 * ... and more
 *)

(* ======================================================================
 * History:
 *
 * $Log: wd_encoding.mli,v $
 * Revision 3.6  2003-03-21 14:23:44  stolpmann
 * 	ocamldoc updated
 *
 * Revision 3.5  2003/03/21 12:50:31  stolpmann
 * 	Fix: encode_as_js_longstring can cope with UTF8-encoded strings
 *
 * Revision 3.4  2003/02/21 21:21:44  stolpmann
 * 	js encoding: Do not escape characters >= 128 because we
 * do not know the character set.
 *
 * Revision 3.3  2002/02/16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:23  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 2.2  2002/01/30 15:12:49  gerd
 * 	HTML encoding: no longer generates entities for characters
 * >= 128. The advantage is that the function now works for both
 * `Enc_iso88591 and `Enc_utf8.
 *
 * Revision 2.1  2002/01/28 02:12:36  gerd
 * 	Initial revision.
 *
 *
 *)
