(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_upload.ml,v 3.2 2002-02-14 16:15:21 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types

type upload_manager = (string, Netcgi.cgi_argument) Hashtbl.t


let get m name =
  Hashtbl.find m name
;;


let init env ia =
  (* Iterate over all entries of ia.uploads, and check whether there is a
   * corresponding CGI argument.
   *)

  let m = Hashtbl.create 10 in

  Wd_interactor.iter
    (fun id name _ _ ->
       (* Note: It should not happen that there are several file upload
	* elements with the same name. But because of defensive programming,
	* we check it here, too.
	*)
       assert (try ignore(Hashtbl.find m name); false
	       with Not_found -> true);
       try
	 (* The name of the CGI argument is formed from the prefix "upload_"
	  * and the numerical ID of the upload interactor.
	  *)
	 let upload_name = "upload_" ^ id in
	 let arg =
	   env.cgi # argument upload_name in
	 (* Put the argument into the hashtable. *)
	 Hashtbl.add m name arg
       with
	   Not_found ->
	     (* Because the CGI argument is missing.
	      * Due to the spec, we add an empty pseudo-argument.
	      *)
	     let arg = new Netcgi.simple_argument name "" in
	     Hashtbl.add m name (arg :> Netcgi.cgi_argument)
    )
    ia.ui_uploads;

  m
;;


(* ======================================================================
 * History:
 *
 * $Log: wd_upload.ml,v $
 * Revision 3.2  2002-02-14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:22  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.4  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.3  2000/09/21 15:12:34  gerd
 * 	Updated for O'Caml 3 and PXP
 *
 * Revision 1.2  2000/05/08 17:56:22  gerd
 * 	Changed such that arbitrary strings can be used as interactor
 * IDs as well as automatically generated numbers.
 *
 * Revision 1.1  2000/04/17 10:11:05  gerd
 * 	Initial revision.
 *
 *
 *)
