(*
 * <COPYRIGHT>
 * Copyright 2005 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_init_expr.mli,v 3.1 2006-03-08 00:56:45 stolpmann Exp $ *)

open Wd_types

val parse_init_expr : Pxp_types.rep_encoding -> Pxp_types.event list -> init_expr
  (** Parses the XML tree describing an initializing expression. The tree
    * must be given as list of XML events corresponding to one of the
    * ui:*-value elements. The list must only contain [E_start_tag],
    * [E_end_tag] and [E_char_data] events, and must end with 
    * [E_end_of_stream]. Ignorable whitespace must already be removed.
    *
    * Raises [Formal_user_error] on syntax error.
   *)


val eval_init_expr : dialog_type -> init_expr -> var_value
  (** Evaluates an initializing expression.
    * Raises [Eval_error] or [Eval_error_noloc] on evaluation error.
    *)


