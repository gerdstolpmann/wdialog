(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_transform.mli,v 3.3 2002-02-16 17:29:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** This is the processor transforming the ui file to HTML output. *)

open Wd_types


class syntax_tree : syntax_tree_type
  (** The realization of the syntax tree *)

val parse_uiapplication :
      ?charset:Pxp_types.rep_encoding ->                (* Default: `Enc_iso88591 *)
      string -> application_type
  (** Parses the file whose name is passed to the function, and returns
   * the contents of the ui file as application declaration.
   *
   * E.g. [parse_uiapplication "index.ui"]
   *
   * [~charset]: This argument determines the _internal_ encoding of the
   *   characters. The internal encoding may be different from the
   *   encoding found in the parsed files; if necessary, the characters
   *   are recoded.
   *   This argument determines also the charset of the returned
   *   application, and thus indirectly:
   *   - The charset of dialog variables and other state data
   *   - The charset of the generated HTML pages
   *)

val load_uiapplication :
      ?charset:Pxp_types.rep_encoding ->                (* Default: `Enc_iso88591 *)
      string -> application_type
  (** Loads the ui definition contained in the binary file, and
   * returns the contents as application declaration.
   *
   * E.g. [load_uiapplication "index.ui.bin"]
   *
   * [~charset]: See also [parse_uiapplication]. The charset MUST be
   *    the same as the charset used in the compiled binary. There
   *    is no check whether this is actually true.
   *)

val compile : ?charset:Pxp_types.rep_encoding -> string -> out_channel -> unit
  (** Compiles the file and writes it to the [out_channel]. The compiled file
   * can later be loaded by [load_uiapplication] which is much faster than
   * [parse_uiapplication].
   *
   * [~charset]: See also [parse_uiapplication]. This charset determines the
   *    charset used in the compiled binary. It must be the same as
   *    the charset used to [load_uiapplication]
   *)


(**/**)

val escape_html : string -> string
  (* [Auxialiary function:] Encodes the special characters by HTML entities
   * E.g. escape_html "<>" = "&lt;&gt;"
   *)
  (* DEPRECATED. Use the functions in Wd_encoding instead *)


val to_text :
      ?context:       syntax_tree_type dict ->
      ?vars:          trans_vars ->
      (* dialog: *)   dialog_type ->
      (* channel: *)  Netchannels.out_obj_channel ->
      (* node: *)     syntax_tree_type ->
	unit
  (** [to_text ~context ~vars dialog channel node]:
   * 
   * Prints the subtree [node] as plain text to the passed [channel].
   * Note that for many node types there are no special plain text handlers,
   * in which case the node is ignored and printing continues with the
   * subnodes of the problematic node.
   *
   * [to_text] is mainly used to get the replacement text for
   * $-parameters occurring in attribute values.
   *)


val to_html :
      ?context:       syntax_tree_type dict ->
      ?vars:          trans_vars ->
      (* dialog: *)   dialog_type ->
      (* channel: *)  Netchannels.out_obj_channel ->
      (* node: *)     syntax_tree_type ->
	unit
  (** [to_html ~context ~vars dialog channel node]:
   * Prints the subtree [node] as HTML text into the passed [channel]. In
   * [dialog] the dialog object must be passed.
   *)


val instantiate :
      ?context:      syntax_tree_type dict ->
      ?params:       syntax_tree_type dict ->
      (* dialog:*)   dialog_type ->
      (* template:*) template_type ->
        syntax_tree
  (** [let tree = instantiate ~context ~params dlg tmpl]:
   *
   *Instantiates the [tmpl] (which must implement instantiation
   * such as a [ui:template] node), and returns the instantiated node.
   *
   * - [~context]:  Contains the bindings of context parameters
   * - [~params]:   Contains the bindings of parameters passed by [ui:param]
   * - [dialog]:   The object used for looking up templates that referred to
   *            within the passed template
   * - [tmpl]: A syntax_tree node which can be instantiated
   *
   * The function raises [Instantiation_error] if something goes wrong.
   *)

val mk_use_param_node :
      (* dtd: *)    Pxp_dtd.dtd ->
      (* name: *)   string ->
      (* params: *) (string * syntax_tree_type) list ->
	                                       (* List of (pK, xK) pairs *)
	syntax_tree
  (* Creates a node
   * <ui:use template="name">
   *   <ui:param name="p1">x1</ui:param>
   *   <ui:param name="p2">x2</ui:param>
   *   ...
   *   <ui:param name="pN">xN</ui:param>
   * </ui:use>
   *)

val mk_text_node :
      (* dtd: *)  Pxp_dtd.dtd ->
      (* text: *)  string ->
	syntax_tree
  (* Creates a text data node *)

val mk_html_node :
      (* dtd: *)   Pxp_dtd.dtd ->
      (* text: *)  string ->
        syntax_tree
  (* Creates an HTML data node *)

val concat :
      ?sep:       syntax_tree ->
      (* dtd: *)  Pxp_dtd.dtd ->
                  syntax_tree list ->
	syntax_tree
  (* Creates a node which ... *)


val pxp_spec : unit -> syntax_tree Pxp_document.spec
  (* Returns the specification of the PXP model *)


(* ======================================================================
 * History:
 *
 * $Log: wd_transform.mli,v $
 * Revision 3.3  2002-02-16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:24  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.7  2002/01/30 15:14:29  gerd
 * 	New: ~charset for several functions.
 *
 * Revision 1.6  2002/01/26 22:38:26  gerd
 * 	Changed the type for passing parameters to templates. It is
 * now syntax_tree dict instead of a simple list. Furthermore, the
 * method [instantiate] no longer returns the new context. The new element
 * ui:context creates the new contexts now.
 *
 * Revision 1.5  2002/01/24 23:37:25  gerd
 * 	<ui:template> and <ui:page> ignore whitespace at the beginning
 * and at the end of the list of subnodes.
 * 	Templates can be studied.
 * 	On instantiation, the special exception [Instantiation_error]
 * is raised when an error happens. So the caller can reports its own
 * line number.
 *
 * Revision 1.4  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.3  2000/12/06 17:52:25  gerd
 * 	New: compile
 *
 * Revision 1.2  2000/12/06 15:29:52  gerd
 * 	New: mk_html_node
 *
 * Revision 1.1  2000/11/30 18:29:07  gerd
 * 	Initial revision
 *
 *)
