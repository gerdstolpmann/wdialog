(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_cycle.mli,v 3.6 2003-03-21 14:23:44 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** The scope of the module [Wd_cycle] is the whole CGI cycle, from the 
  * moment when the CGI request has just arrived until the moment just before
  * the CGI response is delivered to the client.
  *)


open Wd_types

val process_request :
      ?session_manager:session_manager_type ->  (* default: instant_session_manager *)
      ?self_url:string ->
      ?response_header:response_header ->
      universe_type ->
      Netcgi.cgi_activation ->
	unit
 (** This is the main function processing requests coming from the browser.
   * It expects a CGI environment, interprets the CGI variables and performs
   * one request cycle:
   * - The old object is restored (deserialized)
   * - The variables are updated that are bound to interactors, i.e.
   *   the user modifications are propagated to these variables
   * - The current event is determined by checking which button or
   *   link has been clicked
   * - The method  [handle] of the object is invoked
   * - The result of [handle] is interpreted
   * - The method [prepare_page] is invoked on the (probably next) object
   * - The HTML code for the selected page is generated
   * The resulting HTML code is written to the output channel of the
   * [cgi_activation] object passed to this function.
   *
   * For more details of the request cycle, see the documents
   * standard-cycle.txt and popup-cycle.txt.
   *
   * @param session_manager The session manager to use. The main task of the
   *   session manager is to encode/decode the CGI variable "uiobject_session".
   *   The default value is {!Wd_dialog.instant_session_manager}.
   *
   * @param self_url The URL used in the recursive POST requests. It defaults
   *   to the URL returned by the [url] method of the [cgi_activation] object.
   *
   * @param response_header The HTTP response header to use. This header
   *   record can be modified while the request is processed (see the
   *   [response_header] field of the [environment] record).
   *)

val make_environment : Netcgi.cgi_activation -> environment
  (** Creates an (otherwise empty) environment with the passed CGI
   * activation object.
   *)

(* ======================================================================
 * History:
 *
 * $Log: wd_cycle.mli,v $
 * Revision 3.6  2003-03-21 14:23:44  stolpmann
 * 	ocamldoc updated
 *
 * Revision 3.5  2003/01/26 17:59:46  stolpmann
 * 	new: Wd_cycle.make_environment
 *
 * Revision 3.4  2003/01/04 21:55:25  stolpmann
 * 	new record response_header
 *
 * Revision 3.3  2002/02/16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:22  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.4  2002/02/05 18:46:39  gerd
 * 	[process_request] accpepts the session_manager as optional
 * argument.
 *
 * Revision 1.3  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.2  2000/12/06 15:25:59  gerd
 * 	Support for the ~self_url parameter.
 *
 * Revision 1.1  2000/11/30 18:29:07  gerd
 * 	Initial revision
 *
 *)
