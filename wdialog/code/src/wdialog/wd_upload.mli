(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_upload.mli,v 3.4 2003-03-21 14:23:44 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)


(** This module manages file upload parameters. *)

type upload_manager
  (** Manages the file upload parameters of a certain request *)

val get : upload_manager -> string -> Netcgi.cgi_argument
    (** Returns the CGI argument containing the file upload information for
     * the file upload box with the passed name. This name is what is
     * specified in the "name" attribute of ui:file.
     * 
     * See netcgi.mli for accessor functions for [cgi_argument] values.
     * 
     * This function raises [Not_found] if there is no ui:file box with
     * the passed name. This function returns a pseudo argument with
     * empty value, empty filename, and empty MIME type if the browser did
     * not send the corresponding CGI argument to the server. (Note:
     * At least Netscape browsers always send CGI arguments even if the
     * user did not specify files to upload, and these arguments
     * have empty value, empty filename, and empty MIME type. It is a
     * good idea to check for an empty filename in order to find out
     * whether the upload box was used or not.)
     *)

val init : Wd_types.environment -> Wd_types.interactors -> upload_manager
    (** Scans the available CGI parameters for file uploads, and initializes
     * this module.
     * - It is required that the CGI module is already initialized
     * (in [environment])
     * - The argument of this function is the interactor definition of the
     * page that has been submitted.
     *)

(* ======================================================================
 * History:
 *
 * $Log: wd_upload.mli,v $
 * Revision 3.4  2003-03-21 14:23:44  stolpmann
 * 	ocamldoc updated
 *
 * Revision 3.3  2002/02/16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:28  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.2  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.1  2000/04/17 10:11:05  gerd
 * 	Initial revision.
 *
 *
 *)
