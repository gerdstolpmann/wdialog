(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_universe.mli,v 3.3 2002-02-16 17:29:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** Contains the implementation of {!Wd_types.universe_type} *)

open Wd_types

class universe : application_type -> universe_type
  (** Implements the universe. A universe is always bound to a certain
   *  application, so you must pass this application on creation
   *)

(* ======================================================================
 * History:
 *
 * $Log: wd_universe.mli,v $
 * Revision 3.3  2002-02-16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:28  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.3  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.2  2000/12/06 15:29:34  gerd
 * 	New: reset
 *
 * Revision 1.1  2000/04/13 17:42:58  gerd
 * 	Initial revision.
 *
 *
 *)
