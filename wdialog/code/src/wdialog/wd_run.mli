(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_run_fcgi.mli,v 1.1 2004-12-04 03:51:15 gremlin43820 Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** This module contains a customizable main program for application servers
    using any Netcgi2 connector
 *)

open Wd_types
open Netchannels
open Netcgi_fcgi

val create_request_handler :
      ?charset:Pxp_types.rep_encoding ->      (* default: `Enc_iso88591 *)
      ?session_manager:session_manager_type -> (* defalt: instant_session_manager *)
      ?no_cache:bool ->                       (* default: true *)
      ?error_page:(out_obj_channel -> exn -> unit) ->  (* default: a simple error page *)
      ?response_header:response_header ->     (* default: see below *)
      ?reg:(universe_type -> unit) ->
      uifile:string ->
      unit -> (Netcgi.cgi_activation -> unit)

  (** This function creates a request handler for Netcgi2-based application
   * server. Use this function like in this example for FastCGI:
   *
   * Netcgi_fcgi.run
   * (Wd_run.create_request_handler
   *    ~session_manager: (new memory_session_manager)
   *    ~charset:`Enc_utf8 ~reg ~uifile:"adder.ui" ())
   *
   * This main program creates a single threaded (single process) server
   * which handles requests sequentially. Since fastcgi processes are managed
   * by an application server, this is the only mode provided. The application
   * server can then be configured to create process pools, cgi style
   * servers, etc.. See the manual for your fastcgi application server for details.
   *
   * The application is accessible under the URL which you have configured
   * for it in your web server.
   *
   * Call the function [create_request_handler] as follows:
   *
   * {[let req_hdl = create_request_handler ~reg ~uifile ()]}
   *
   * The argument [~reg] registers the dialog classes (like in Wd_run_cgi).
   * The argument [~uifile] must be the absolute path of the UI definition.
   * The suffix of this file must be ".ui" or ".ui.bin".
   *
   * @param charset determines the character set for the internal representation
   *   of strings and for the generated HTML pages. (Note that the parsed
   *   XML files can have character sets independent of this setting.)
   *   The default is [ISO-8859-1].
   *
   * @param no_cache If [true]: The CGI response contains header directives
   *   preventing the browser (and all caches between server and browser)
   *   from storing the response. This ensures that never a cache copy
   *   is taken, and for many browsers, the history is no longer usable
   *   (when hitting the "Back" button, the user only gets a message
   *   that the data is expired, but not the old page).
   *   The default is [true].
   *
   * @param error_page This function is called when an exception is caught.
   *   It can generate an error page by writing a nicely formatted
   *   message to the output channel. The default is a function that
   *   outputs a simple error page.
   *
   * @param response_header If set, this parameter is taken as the initial
   *   HTTP response header while processing the request. If omitted, 
   *   the response header defaults to: The content type is text/html,
   *   the character set as determined by [charset], the caching header is
   *   set according to [no_cache].
   *)


(* ======================================================================
 * History:
 *
 * $Log: wd_run_fcgi.mli,v $
 * Revision 1.1  2004-12-04 03:51:15  gremlin43820
 * finially adding fastcgi support
 *
 * Revision 1.3  2003/03/21 14:23:44  stolpmann
 * 	ocamldoc updated
 *
 * Revision 1.2  2003/01/04 21:55:25  stolpmann
 * 	new record response_header
 *
 * Revision 1.1  2002/03/19 22:09:55  stolpmann
 * 	Initial revision.
 *
 *)
