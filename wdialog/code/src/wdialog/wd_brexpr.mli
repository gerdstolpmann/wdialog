(*
 * <COPYRIGHT>
 * Copyright 2005 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_brexpr.mli,v 3.1 2006-03-08 00:56:45 stolpmann Exp $ *)

(** Parsing of bracket expressions *)

(* Dep info: This interface precedes Wd_types *)

type fn_name =
    [ `Expr_fun_name of string
	(** A function name *)
    ]

type 'a expr_ap =
    [ `Expr_var of string                      
	(** a variable in an expr *)
    | `Expr_strconst of string
        (** a string constant *)
    | `Expr_apply of ( 'a * 'a expr_ap list)      
	(** a function call [(fun_name, args)] *)
    | `Expr_param of (string * string list)    
	(** a template parameter [(param_name, oe_list)] *)
    | `Expr_matrix of (string * 'a expr_ap) list list
	(** a matrix [ [  [(col_name, cell_value); ...]; next row; ... ]].
         * It is not checked here whether the matrix is well-formed.
	 *)
    | `Expr_fun of string list * 'a expr_ap          
	(** a lambda [(var_names, expr)] *)
    | `Expr_let of (string * 'a expr_ap) list * 'a expr_ap 
	(** a let binding [ ( [var_name, var_value; ...], expr ) ] *)
    ]
  (** Expressions inside [$[...]] ('a will be [expr_fn] below) *)

type expr_fn =
    [ fn_name | 'a expr_ap ] as 'a
      (** Just a type abbreviation *)

type expr = expr_fn expr_ap
  (** Expressions inside [$[...]], now closed *)

type expr_oe =
    [ `Expr_oe of expr * string list  (** expression with output encs *)
    | expr
    ]

type expr_string = (expr_string_item * string) list
  (** A string literal with occurrences of [$[...]] or [${...}].
    * The pair [(e,s)] means that the input string [s] is interpreted as
    * [e].
   *)

and expr_string_item =
   [ `Literal of string   (** non-empty string literal (occurs verbatim in source text) *)
   | expr_oe              (** expression inside the string *)
   ]
  (** An item of an [expr_string] is either a literal (constant), or
    * [$[...]] or [${...}].
   *)

val parse_expr : enable_param:bool -> Pxp_types.rep_encoding -> string -> 
                 expr_oe
  (** Parses the expression within [$[...]]. If [enable_param], the expression
    * may contain template parameters.
    *
    * Raises [Formal_user_error] when a syntax error is detected.
    *)

val parse_expr_string : enable_param:bool -> enable_brexpr:bool -> 
                        Pxp_types.rep_encoding -> string -> expr_string
  (** Parses a string with occurrences of [$[...]] or [${...}]. If
   * [enable_param], the string may contain template parameters. If
   * [enable_brexpr], the string may contain bracket expressions.
   * If both are false, occurrences of "$$" are still parsed (i.e. the
   * dollar character remains a meta character).
   *
   * Raises [Formal_user_error] when a syntax error is detected.
   *)

val params_in_expr : expr_oe -> string list 
val params_in_expr_string : expr_string -> string list
  (** Return the referenced template parameters *)


val subst_expr_params : 
  subst:(string * string list -> string) -> expr_oe -> expr_oe
val subst_expr_string_params : 
  subst:(string * string list -> string) -> expr_string -> expr_string
  (** Substitute the [`Expr_param p] items by [`Expr_strconst s] where
    * [s = subst p].
   *)

(* TODO: beta recduction etc. *)
