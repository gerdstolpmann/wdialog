(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_types.ml,v 3.11 2006-03-08 00:56:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

type (+'a) dict = 'a Wd_dictionary.t

type event =
    Button of string
  | Image_button of (string * int * int)
  | Indexed_button of (string * string)
  | Indexed_image_button of (string * string * int * int)
  | No_event
  | Popup_request of string


type interactors =
    { mutable ui_buttons :             string option Wd_interactor.t;
      mutable ui_imagebuttons :        string option Wd_interactor.t;
      mutable ui_anchors :             string option Wd_interactor.t;
      mutable ui_indexed_buttons :     string option Wd_interactor.t;
      mutable ui_indexed_imagebuttons :string option Wd_interactor.t;
      mutable ui_indexed_anchors :     string option Wd_interactor.t;
      mutable ui_vars :                unit Wd_interactor.t;
      mutable ui_enumvars :            (string * string option * string) list;
      mutable ui_uploads :             unit Wd_interactor.t;
    }


type head = string array


type init_expr =
    [ `String_iexpr of Wd_brexpr.expr_string
    | `Enum_iexpr of Wd_brexpr.expr_string list
    | `Dyn_enum_iexpr of (Wd_brexpr.expr_string * Wd_brexpr.expr_string) list
    | `Alist_iexpr of (Wd_brexpr.expr_string * init_expr) list
    | `Matrix_iexpr of head * Wd_brexpr.expr_string array array
    | `Value_iexpr of Wd_brexpr.expr
    | `Map_iexpr of Wd_brexpr.expr * init_expr list
    | `Dsrc_iexpr of string * Wd_brexpr.expr_string
    ]


class type table =
object
  method head : head
  method select : column:string -> string -> table
  method card : int
  method iter : unit -> iterator option
end

and iterator =
object
  method row : string array
  method large_row : string Stream.t array
  method next : iterator option
  method close : unit -> unit
end



type 'dlg poly_var_value =
    String_value   of string
  | Enum_value     of string list
  | Dialog_value   of 'dlg option
  | Dyn_enum_value of (string * string) list
  | Alist_value    of (string * 'dlg poly_var_value) list
  | Matrix_value   of head * string array array
  | Table_value    of table
  | Function_value of ('dlg -> 
			 'dlg poly_var_value Lazy.t list -> 'dlg poly_var_value)


type enum_decl =
    { enum_name : string;
      mutable (* XXX *) enum_definition : (string * string) list;
    }


type var_type_name =
    String_type
  | Enum_type of enum_decl
  | Dialog_type
  | Dyn_enum_type
  | Matrix_type
  | Table_type 


type 'dlg poly_var_decl =
    { var_name : string;
      var_type : var_type_name;
      var_init : init_expr option;
      var_temporary : bool;
      var_associative : bool;
      var_protected : bool;
    }


type response_header =
    { mutable rh_status : Nethttp.http_status;
      mutable rh_content_type : string;
      mutable rh_cache : Netcgi.cache_control;
      mutable rh_filename : string option;
      mutable rh_language : string option;
      mutable rh_script_type : string option;
      mutable rh_style_type : string option;
      mutable rh_set_cookie : Nethttp.cookie list;
      mutable rh_fields : (string * string list) list;
    }

type debug_mode_style =
  [ `Fully_encoded
  | `Partially_encoded
  ]

type environment =
    { (* Overall way of operation: *)
      debug_mode : bool;
      debug_mode_style : debug_mode_style;
      prototype_mode : bool;
      server_popup_mode : bool;
      (* Web variables *)
      self_url : string;
      response_header : response_header;
      cgi : Netcgi.cgi_activation;
    }


type trans_vars =
    { mutable within_popup : bool;
      mutable current_page : string;
      mutable popup_env_initialized : bool;
      mutable condition_code : bool;
              (* last result of <ui:if>, <ui:ifvar>, <ui:iflang> *)
      mutable serialize_session : unit -> string;
    }
    (* Some local variables that are used in the recursive transformation.
     * By putting them all into a record it is simpler to pass them to the
     * subsequent transformers.
     *)


type ('universe,'dialog,'environment) poly_ds_buf = 
    ('universe,'dialog,'environment) Wd_serialize_types.poly_ds_buf =
    { ds_str : string;         (* String to deserialize *)
      mutable ds_pos : int;    (* Current position *)
      ds_end : int;            (* End position *)
      ds_universe : 'universe;
      ds_environment : 'environment;
      ds_dialogs : (int, 'dialog) Hashtbl.t;   
         (* maps dialog IDs to dialogs *)
    }


exception Not_found_in_catalog of string * string * string


class type message_catalog =
object
  method domain : string
  method lookup : language:string -> string -> string
  method uplinks : string list
  method as_table : table
end


class type extensible_message_catalog =
object
  inherit message_catalog
  method add_message : language:string -> string -> string -> unit
end


class type dialog_decl_type =
  object ('self)
    (* PUBLIC *)
    method name : string
    method enumeration : string -> enum_decl
    method variable_names : string list
    method variable : string -> dialog_type poly_var_decl
    method page_names : string list
    method page : string -> syntax_tree_type
    method page_is_declared_as_popup : string -> bool
    method start_page : string
    method default_context : syntax_tree_type dict
    method language_variable : string option
    (* RESTRICTED *)
    method set_name : string -> unit
    method set_start_page : string -> unit
    method add_enumeration : enum_decl -> unit
    method add_variable : dialog_type poly_var_decl -> unit
    method add_page : string -> syntax_tree_type -> unit
    method set_default_context : syntax_tree_type dict -> unit
    method set_language_variable : string -> unit
  end

and virtual dialog_type =
  object ('self)
    method copy : dialog_type
    method name : string
    method page_name : string
    method page_names : string list
    method variable : string -> dialog_type poly_var_value
    method variable_decl : string -> dialog_type poly_var_decl
    method initial_variable_value : string -> dialog_type poly_var_value
    method string_variable : string -> string
    method enum_variable : string -> string list
    method dyn_enum_variable : string -> (string * string) list
    method dialog_variable : string -> dialog_type option
    method alist_variable : string -> (string * dialog_type poly_var_value) list
    method matrix_variable : string -> head * string array array
    method table_variable : string -> table
    method function_variable : 
      string -> 
      ( dialog_type -> 
	  dialog_type poly_var_value Lazy.t list -> 
	    dialog_type poly_var_value )
    method lookup_string_variable : string -> string -> string
    method lookup_enum_variable : string -> string -> string list
    method lookup_dyn_enum_variable : string -> string -> (string * string) list
    method lookup_dialog_variable : string -> string -> dialog_type option
    method lookup_matrix_variable : string -> string -> head * string array array
    method lookup_table_variable : string -> string -> table
    method lookup_function_variable : 
      string ->
      string -> 
      ( dialog_type -> 
	  dialog_type poly_var_value Lazy.t list -> 
	    dialog_type poly_var_value )
    method set_variable : string -> dialog_type poly_var_value -> unit
    method unset_variable : string -> unit
    method lookup_uploaded_file : string -> Netcgi.cgi_argument option
    method dump : Format.formatter -> unit
    method next_page : string
    method set_next_page : string -> unit
    method event : event
    method is_server_popup_request : bool
    method serialize : (dialog_type,unit) Hashtbl.t -> Buffer.t -> unit
    method unserialize : (universe_type,dialog_type,environment) poly_ds_buf -> unit

    method enter_session_scope : session_type -> unit
    method leave_session_scope : unit -> unit
    method session : session_type

    method environment : environment
    method declaration : dialog_decl_type
    method application : application_type
    method universe : universe_type

    (*********************************************************************)

    (* The following methods needs to be defined in subclasses: *)

    method virtual prepare_page : unit -> unit
    method virtual handle : unit -> unit

    (*********************************************************************)

    (* Internal methods *)

    method set_event : event -> unit
    method set_server_popup_request : unit -> unit
    method init : string -> unit
    method lock_ia : bool -> unit
    method interactors : interactors

    (* -- class [dialog] also has a private method:
     * method private put_tree : string -> Template.tree -> unit
     *)

  end

and application_type =
  object ('self)
    (* PUBLIC *)
    method start_dialog_name : string
    method dialog_names : string list
    method dialog_declaration : string -> dialog_decl_type
    method template_names : string list
    method template : string -> template_type
    method study : unit -> unit
    method output_encoding : string -> (string -> string)
    method var_function : 
             string -> 
               ( dialog_type -> 
		 dialog_type poly_var_value list -> 
		   dialog_type poly_var_value)
    method lazy_var_function : 
             string -> 
               ( dialog_type -> 
		 dialog_type poly_var_value Lazy.t list -> 
		   dialog_type poly_var_value)
    method dtd : Pxp_dtd.dtd
    method charset : Pxp_types.rep_encoding
    method debug_mode : bool
    method debug_mode_style : debug_mode_style
    method prototype_mode : bool
    method onstartup_call_handle : bool
    method add_var_function : 
             string -> 
	     ( dialog_type ->
	       dialog_type poly_var_value list -> 
		 dialog_type poly_var_value) ->
	       unit
    method add_lazy_var_function : 
             string -> 
	     ( dialog_type -> 
	       dialog_type poly_var_value Lazy.t list -> 
		 dialog_type poly_var_value) ->
	       unit
    method catalog : string -> message_catalog
    method add_catalog : message_catalog -> unit
    method add_extensible_catalog : extensible_message_catalog -> unit
    method add_message : 
      domain:string -> language:string -> string -> string -> unit
    method lookup_message :
      domain:string -> language:string -> string -> string
    method data_source : string -> data_source
      (** Gets a data source object or raises [Not_found] *)
    method data_sources : data_source list
    method add_data_source : data_source -> unit

    (* RESTRICTED *)
    method set_start_dialog_name : string -> unit
    method add_dialog_declaration : dialog_decl_type -> unit
    method add_template : ?lib:bool -> string -> template_type -> unit
    method add_output_encoding : string -> (string -> string) -> unit
    method set_debug_mode : bool -> debug_mode_style -> unit
    method set_prototype_mode : bool -> unit
    method set_onstartup_call_handle : bool -> unit
  end

and template_type =
  object
    method study : application_type -> unit
    method instantiate :
             ?context: syntax_tree_type dict ->
	     ?vars: trans_vars ->
             ?params:  syntax_tree_type dict ->
             dialog_type ->
               syntax_tree_type
      (* Note: Raises Instantiation_error if something goes wrong *)
  end

and syntax_tree_type =
  object ('self)
    (* PXP *)
    inherit [syntax_tree_type Pxp_document.node] Pxp_document.extension
    (* TEMPLATES *)
    inherit template_type
    (* SCANNERS *)
    (* The side-effect of the scanners is to put the result into the passed
     * argument.
     *)
    method scan_application : application_type -> unit
    method scan_dialog : application_type -> dialog_decl_type -> unit
    method scan_enumeration : enum_decl -> unit
    (* ACCESS *)
    method escaped_data : string
    (* OUTPUT *)
    method to_html :
      ?context: syntax_tree_type dict ->
      ?vars:    trans_vars ->
      dialog_type ->
      Netchannels.out_obj_channel ->
	unit
    (* TODO: to_xhtml *)
    method to_text :
      ?context: syntax_tree_type dict ->
      ?vars:    trans_vars ->
      dialog_type ->
      Netchannels.out_obj_channel ->
	unit
  end

and universe_type =
  object ('self)
    method application : application_type
    method register : string -> (universe_type -> string -> environment -> dialog_type) -> unit
    method create : environment -> string -> dialog_type
  end

and session_manager_type =
  object
    method create : dialog_type -> session_type
    method unserialize : universe_type -> environment -> string -> session_type
  end

and session_type =
object
    method session_id : string
    method dialog_name : string
    method dialog : dialog_type
      (* Returns the dialog *)
    method commit_changes : unit -> unit
      (* Causes that [serialize] returns the current state of the dialog *)
    method serialize : string
      (* Returns the state of the dialog at the time of the last [commit_change]
       * or the state of the initial dialog
       *)
    method change_dialog : dialog_type -> unit
      (* Continue with a new dialog. The methods [dialog_name] and [dialog] will
       * immediately reflect the change. However, you have to call [commit_changes]
       * to make [serialize] return the state of the new dialog.
       *)
  end


and data_source =
object
  method name : string
  method pre_handle : dialog_type -> unit
  method post_handle : dialog_type -> bool -> unit
  method pre_prepare_page : dialog_type -> unit
  method post_prepare_page : dialog_type -> bool -> unit
  method retrieve : string -> table
end


type var_value = dialog_type poly_var_value
type var_decl  = dialog_type poly_var_decl
type ds_buf = (universe_type,dialog_type,environment) poly_ds_buf

exception Change_dialog of dialog_type
  (* The implementation of the 'handle' method of a dialog may raise
   * Change_dialog to drop the current dialog and continue with another
   * dialog.
   *)

exception Change_page of string
  (* The implementation of the 'handle' method of a dialog  may raise
   * Change_page to set the next page to display for the current dialog.
   *)

exception Formal_user_error of string
exception Runtime_error of string
exception No_such_variable of string

exception Instantiation_error of string
exception Eval_error of string
exception Eval_error_noloc of string

exception During_action of string * exn


let revision_types = "$Revision: 3.11 $" ;;
  (* Intentionally the CVS revision string! *)

(* ======================================================================
 * History:
 *
 * $Log: wd_types.ml,v $
 * Revision 3.11  2006-03-08 00:56:45  stolpmann
 * Addition of Table_value, Matrix_value and Function_value.
 *
 * New parser for initial expressions. It is now possible to
 * use $[...] in variable initializers.
 *
 * Extended bracket expressions: functions, let expressions,
 * word literals, matrix literals.
 *
 * New type for message catalogs, although not yet implemented.
 *
 * Revision 3.10  2005/12/09 20:02:40  stolpmann
 * Fix in Wd_interactor.unserialize.
 *
 * The [interactors] are now locked in the "handle" stage of processing.
 * Any trial will fail.
 *
 * Revision 3.9  2005/06/11 14:24:14  stolpmann
 * Extension of bracket expressions: many new functions.
 * Functions in bracket expressions may now lazily evaluate their arguments.
 * ui:if and ui:ifvar may refer to any functions defined for bracket
 * expressions.
 * New: ui:ifexpr
 * Parsing of bracket expressions is now done with ulex. Private interfaces
 * of PXP are no longer used for this purpose.
 * Serialization has been rewritten, and it is now safe.
 * Fix: In ui:context there may now be macro references at any location.
 * Also documented all these changes.
 *
 * Revision 3.8  2004/12/12 17:57:32  stolpmann
 * 	Added <q:wd-link> and <q:wd-xlink> to generate links for
 * applications that cannot use Javascript. Limited functionality, however.
 * See stdlib.xml for details.
 *
 * Revision 3.7  2003/02/16 23:48:15  stolpmann
 * 	Improved wd-debug-mode: there are now two styles
 *
 * Revision 3.6  2003/01/04 21:55:25  stolpmann
 * 	new record response_header
 *
 * Revision 3.5  2002/11/09 11:41:19  stolpmann
 * 	Fix: ui:select accepts dot notation. A new method
 * variable_decl needs to be defined for dialogs. This method
 * returns the var_decl record and interprets the dot notation.
 *
 * Revision 3.4  2002/10/20 19:39:17  stolpmann
 * 	New feature: The brackets $[...] can contain expressions,
 * not only variables
 *
 * Revision 3.3  2002/04/10 20:04:05  stolpmann
 * 	The CVS revision number is exported.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:21  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 2.9  2002/02/11 11:14:21  gerd
 * 	Fix: It is now possible to change the dialog of sessions (required
 * for the Change_dialog exception)
 *
 * Revision 2.8  2002/02/07 18:49:59  gerd
 * 	Standard library
 *
 * Revision 2.7  2002/02/05 18:43:04  gerd
 * 	New: var_protected
 * 	New: page_is_declared_as_popup
 * 	New: debug_mode, prototype_mode, onstartup_call_handle
 * 	New: Session management
 *
 * Revision 2.6  2002/01/31 23:06:40  gerd
 * 	Revised conditional expansion (ui:if, ui:ifvar, ui:iflang,
 * ui:cond).
 * 	Added some support for internationalization (xml:lang).
 *
 * Revision 2.5  2002/01/30 15:16:10  gerd
 * 	New: charset
 * 	New argument application_type for [study]
 * 	New argument application_type for [scan_dialog]
 *
 * Revision 2.4  2002/01/28 02:12:54  gerd
 * 	Added support for output encodings.
 *
 * Revision 2.3  2002/01/26 22:38:27  gerd
 * 	Changed the type for passing parameters to templates. It is
 * now syntax_tree dict instead of a simple list. Furthermore, the
 * method [instantiate] no longer returns the new context. The new element
 * ui:context creates the new contexts now.
 *
 * Revision 2.2  2002/01/24 23:35:18  gerd
 * 	New exception: [Instantiation_error]
 *
 * Revision 2.1  2002/01/21 14:29:04  gerd
 * 	Initial revision.
 *
 *
 *)
