(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_template.mli,v 3.5 2003-03-21 20:57:54 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)


(** User access to the template definitions *)

open Wd_types

exception Template_not_found of string
  (** Raised when a template cannot be found. 
   * The argument is the name of the missing template 
   *)

type template
  (** The type of template definitions *)

type tree
  (** The type of instantiated templates ("XML trees") *)

(** Many of the following functions either have an {!Wd_types.application_type}
 *  object as first argument or a {!Wd_types.dialog_type} object. These
 * objects contain "background information" that are necessary to do their
 * job.
 *
 * Note that the base dialog class {!Wd_dialog.dialog} defines a number of
 * private methods that correspond to the following functions, but that
 * know the first argument implicitly. For example, you can call 
 * {[self#t_to_string tree]} instead of 
 * {[Wd_template.to_string self tree]}.
 *)

val get : application_type -> string -> template
      (** [get app n]: looks up the template with name [n], or raise
       * [Template_not_found].
       *)

val apply :
       dialog_type ->
       template ->
       (string * tree) list ->
	 tree
      (** [apply dlg t params]: instantiates the template [t] with parameters 
       * [params] and returns the result.
       * If more parameters are supplied than actually referenced in the
       * template, these are silently ignored.
       * If parameters are referenced that are missing in [params], an
       * {!Wd_types.Instantiation_error} happens.
       *)

val apply_byname : 
       ?localized:bool ->
       dialog_type -> 
       string -> 
       (string * tree) list -> 
	 tree
      (** [apply_byname dlg n params = apply (get n) params]: instantiates
       * the template with name [n] with parameters [params] and returns
       * the result. See [apply] for the possible exceptions.
       *
       * [~localized]: If [true] (default), the localized template is
       *   returned, if available. If [false], exactly the template
       *   is returned that has been requested.
       *)

val apply_lazily : dialog_type -> string -> (string * tree) list -> tree
      (** [apply_lazily dlg n params]: creates a tree node which applies the
       * template [n] by instantiating the parameters [params].
       * The tree node is simply a "[<ui:use template=...>...</ui:use>]" node,
       * and it is expanded only when needed.
       *)

val concat : application_type -> tree -> tree list -> tree
      (** [concat app sep l]: if [l = [n1;n2;...;nN]], the concatenation
       * [n1] . [sep] . [n2] . [sep] ... [sep] . [nN] is formed.
       *)
val empty : application_type -> tree
      (** A tree node which expands to the empty string. *)

val text : application_type -> string -> tree
      (** [text app s]: Forms a tree node which expands to the text [s]. This
       * means if [s] contains meta characters (esp. <, >, &) these are
       * converted to their corresponding entities (&lt; &gt; &amp; etc)
       *)

val html : application_type -> string -> tree
      (** [html app s]: Forms a tree node which expands exactly to [s]. This means
       * that if [s] contains HTML meta characters these are left as they are.
       * Because of this it is possible to include HTML elements. The
       * string [s] is not parsed.
       *
       * NOTE: It is {b not} possible to create ui:* elements with this
       * function that will be interpreted. If you want to create such
       * elements you must instantiate templates containing them.
       *
       * Internally, this function creates a [ui:special] node.
       *)

val to_string : dialog_type -> tree -> string
      (** [to_string dlg t]: converts the tree representation [t] to its string
       * representation. You need this function to put a tree into
       * a string variable.
       * You can then insert this variable into your document by using
       * [<ui:dynamic variable="name-of-variable" special="yes"/>].
       *
       * For convenience, dialogs have a method [put_tree] putting a
       * tree into string variable.
       *
       * NOTE: The expansion process assumes that the generated HTML code
       * will be inserted into a main page and {b not} a popup page.
       *)


(* ======================================================================
 * History:
 *
 * $Log: wd_template.mli,v $
 * Revision 3.5  2003-03-21 20:57:54  stolpmann
 * 	ocamldoc problems
 *
 * Revision 3.4  2003/03/21 14:23:44  stolpmann
 * 	ocamldoc updated
 *
 * Revision 3.3  2002/02/16 17:29:45  stolpmann
 * 	mostly ocamldoc.
 *
 * Revision 3.2  2002/02/14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:23  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.5  2002/01/31 23:06:39  gerd
 * 	Revised conditional expansion (ui:if, ui:ifvar, ui:iflang,
 * ui:cond).
 * 	Added some support for internationalization (xml:lang).
 *
 * Revision 1.4  2002/01/24 23:35:30  gerd
 * 	Updated comments
 *
 * Revision 1.3  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.2  2000/12/06 15:28:20  gerd
 * 	to_string: (Still buggy) support for the ~self_url parameter.
 * 	html: Implemented for the first time
 *
 * Revision 1.1  2000/04/13 17:42:58  gerd
 * 	Initial revision.
 *
 *
 *)
