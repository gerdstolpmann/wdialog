(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_universe.ml,v 3.2 2002-02-14 16:15:21 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types

class universe (init_app : application_type) : universe_type =
  object(self)
    val app = init_app
    val registry = Hashtbl.create 50

    method application = app

    method register name creator =
      if Hashtbl.mem registry name then
	failwith ("universe#register: dialog `" ^  name ^  "' is already registered");
      Hashtbl.add registry name creator

    method create env name =
      let creator = Hashtbl.find registry name in
      creator (self : #universe_type :> universe_type) name env
  end
;;

(* ======================================================================
 * History:
 *
 * $Log: wd_universe.ml,v $
 * Revision 3.2  2002-02-14 16:15:21  stolpmann
 * 	Added copyright notice.
 *
 * Revision 3.1  2002/02/12 20:29:21  stolpmann
 * 	Initial release at sourceforge.
 *
 * Revision 1.5  2002/01/14 15:03:24  gerd
 * 	Major change: Typing has been completely revised, and almost
 * every tiny thing has now a new type. Also renamed a lot.
 *
 * Revision 1.4  2000/12/06 15:29:34  gerd
 * 	New: reset
 *
 * Revision 1.3  2000/11/30 18:42:49  gerd
 * 	Does no longer open Parse. (Not needed)
 *
 * Revision 1.2  2000/04/17 10:10:52  gerd
 * 	New Cgi module.
 * 	File upload strongly improved.
 *
 * Revision 1.1  2000/04/13 17:42:58  gerd
 * 	Initial revision.
 *
 *
 *)
