(*
 * <COPYRIGHT>
 * Copyright 2005 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_catalog.mli,v 3.1 2006-03-08 16:05:02 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

(** The default catalog implementation *)

open Wd_types

type parsed_catalog =
    { pc_uplinks : string list;
      pc_messages : (string * string * string) list  (* lang, int, ext triples *)
    }

val parse_catalog : Pxp_types.event list -> parsed_catalog
  (** Parses the <catalog>...</catalog> XML element
   *)

class catalog : ?uplinks:string list -> string -> extensible_message_catalog
  (** [new catalog domain] creates a new catalog identified by [domain].
    * If [uplinks] are given, these are entered into the catalog.
   *)

