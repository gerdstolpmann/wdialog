(*
 * <COPYRIGHT>
 * Copyright 2002 Joachim Schrod Network and Publication Consultance GmbH, Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WDialog.
 *
 * WDialog is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WDialog is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id: wd_brexpr_lex.ml,v 3.1 2006-03-08 00:56:45 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Netulex

(* Scanner for bracket expressions *)

type tok =
  | Token of string
  | Number of int
  | QToken of string
  | Slash
  | Dollar
  | LParen
  | RParen
  | RParenDot
  | LBrace
  | RBrace
  | Comma
  | Semicolon
  | Equal
  | Eof
  | Other of string
      (* Delimiters reserved for the future *)

let rec scan =
  lexer 
  | "/" -> Slash
  | "$" -> Dollar
  | "(" -> LParen
  | ")" -> RParen
  | ")." -> RParenDot
  | "{" -> LBrace
  | "}" -> RBrace
  | "," -> Comma
  | ";" -> Semicolon
  | "=" -> Equal
  | '-'? [ '0'-'9'] + -> Number (int_of_string (Ulexing.utf8_lexeme lexbuf))
  | [ 'A'-'Z' 'a'-'z' '_'  160-0x10ffff ] 
    [ 'A'-'Z' 'a'-'z' '_' '0'-'9' '-' ':' '.' 160-0x10ffff ]* ->
      Token (Ulexing.utf8_lexeme lexbuf)
  | "'" [ 'A'-'Z' 'a'-'z' '_' '0'-'9' '-' ':' '.' 160-0x10ffff ]* -> 
      let s = Ulexing.utf8_lexeme lexbuf in
      QToken (String.sub s 1 (String.length s - 1))
  | [ ' ' '\t' '\r' '\n' ]+ ->
      scan lexbuf
  | _ -> Other (Ulexing.utf8_lexeme lexbuf)
  | eof -> Eof
;;
