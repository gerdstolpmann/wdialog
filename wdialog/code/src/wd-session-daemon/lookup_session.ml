open Wdstated_clnt
open Wdstated
open V1
open Wdstated_aux
open Rpc
open Unix

let connect () = create_portmapped_client Sys.argv.(1) Tcp

let _ = match get_session (connect ()) (Sys.argv.(2), Sys.argv.(3), Sys.argv.(4)) with
    {result_code=0l;serialized_data=data} -> print_endline data
  | _ -> prerr_endline "error"
