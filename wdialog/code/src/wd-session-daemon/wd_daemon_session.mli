exception Session_not_found
exception Failed_to_commit_changes of int
exception Cannot_fetch_dialog of int
exception Failed_to_get_session of int

(** new daemon_session_manager sessiond_user sessiond_pass host session_timeout *)
class daemon_session_manager : string -> string -> string -> 
  int -> Wd_types.session_manager_type
