typedef string ustring<>;

struct st_get_result {
	int result_code;
	ustring serialized_data;
};
typedef st_get_result get_result;

program wdstated {
	version V1 {
		/* int put_session(ustring user,
		                   ustring pass,
				   int timeout,
                                   ustring key,
                                   ustring serialized_data) */
		int put_session(ustring,
				ustring,
				int,
				ustring,
				ustring) = 1;

		/* int replace_session(ustring user,
		                       ustring pass,
				       int timeout,
				       ustring key,
				       ustring serialized_data) */
		int replace_session(ustring,
				    ustring,
				    int,
				    ustring,
				    ustring) = 2;

		/* get_result get_session(ustring user,
		                          ustring pass,
					  ustring key) */
		get_result get_session(ustring, ustring, ustring) = 3;
	} = 1;
} = 0x20000211;
