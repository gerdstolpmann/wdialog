open Inifiles

type config = {
  users: (string, string) Hashtbl.t;
  sweep: int
}

let read_users cfg = 
  let tbl = Hashtbl.create 1 in
    List.iter
      (fun sec -> Hashtbl.add tbl (cfg#getval sec "name") (cfg#getval sec "password"))
      (List.filter
	 (fun sec -> not (sec = "server"))
	 cfg#sects);
    tbl

let getconfig path = 
  let cfg = new inifile path in
    {users=(read_users cfg);
     sweep=(int_of_string (try cfg#getval "server" "sweep" with _ -> "600"))}
