#! /bin/sh
# (*
exec /opt/ocaml-3.04/bin/ocaml "$0" "$@"
*) use "findlib";;

#require "wdialog";;

(* Load the extensible parser camlp4 and the syntax extension wdialog-p4 *)
#camlp4o;;
#require "wdialog-p4";;
#warnings "a";;                      (* Turn warnings off *)

#use "index.ml";;
