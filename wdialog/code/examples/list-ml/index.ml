(* $Id: index.ml,v 3.1 2002-02-12 20:51:06 stolpmann Exp $
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types
open Wd_dialog
open Wd_run_cgi

let read_directory path =
  (* Return the file names of the directory "path" as string list *)

  let d = Unix.opendir path in

  let rec readrec () =
    try
      let name = Unix.readdir d in
	name :: readrec()
    with
	End_of_file -> []
  in
  let names = readrec() in
  Unix.closedir d;
  names
;;


let permissions kind perm =
  (* Compute a textual representation of file 'kind' and
   * permission mode 'perm'
   *)
  let first_letter =
    match kind with
	Unix.S_REG -> "-"
      | Unix.S_DIR -> "d"
      | Unix.S_CHR -> "c"
      | Unix.S_BLK -> "b"
      | Unix.S_LNK -> "l"
      | Unix.S_FIFO -> "p"
      | Unix.S_SOCK -> "s"
  in

  let s = ref "" in
  let p = ref perm in

  List.iter
    (fun letter ->
       begin
	 if !p mod 2 = 1 then
	   s := letter ^ !s
	 else
	   s := "-" ^ !s
       end;
       p := !p / 2)
    [ "x"; "w"; "r"; "x"; "w"; "r"; "x"; "w"; "r" ];

  s := first_letter ^ !s;

  if (perm land 0o1000) > 0 then begin
    if !s.[9] = 'x' then
      !s.[9] <- 't'
    else
      !s.[9] <- 'T'
  end;

  if (perm land 0o2000) > 0 then begin
    if !s.[6] = 'x' then
      !s.[6] <- 's'
    else
      !s.[6] <- 'l'
  end;

  if (perm land 0o4000) > 0 then begin
    if !s.[6] = 'x' then
      !s.[6] <- 's'
    else
      !s.[6] <- 'S'
  end;

  !s
;;


let owner uid =
  (* Converts the numerical uid to a string *)
  try
    (Unix.getpwuid uid).Unix.pw_name
  with
      Not_found -> string_of_int uid
;;


let group gid =
  (* Converts the numerical gid to a string *)
  try
    (Unix.getgrgid gid).Unix.gr_name
  with
      Not_found -> string_of_int gid
;;


let timestring t =
  (* Converts the time value t to a string *)
  let tm = Unix.localtime t in
  Printf.sprintf
    "%04d-%02d-%02d %02d:%02d:%02d"
    (tm.Unix.tm_year + 1900)            (* Y2k-compliant *)
    (tm.Unix.tm_mon + 1)
    (tm.Unix.tm_mday)
    (tm.Unix.tm_hour)
    (tm.Unix.tm_min)
    (tm.Unix.tm_sec)
;;


class directory universe name env =
  object (self)
    inherit Wd_dialog.dialog universe name env

    method prepare_page() =
      (* Compute the variable "listing": *)

      match self # page_name with
	  "show-listing" ->
	    self # prepare_show_listing()
	| _ ->
	    ()

    method private prepare_show_listing() =

      let path = self # string_variable "path" in
      let names = Sort.list
		    (fun a b -> a <= b)
		    (read_directory path) in             (* file names *)

      (* Get all necessary templates: *)
      let t_directory_row = self # t_get "directory-row" in
      let t_filename      = self # t_get "filename" in
      let t_filename_d    = self # t_get "filename_d" in

      let t0 = Unix.gettimeofday() in
      (* Map 'names' to a tree list: *)
      let rows =
	List.map
	  (fun name ->

	     let path_name = Filename.concat path name in

	     let s  = Unix.stat path_name in
	     let ls = Unix.lstat path_name in

	     (* If the file is a directory, use t_filename_d instead of
	      * t_filename:
	      *)
	     let t_fname =
	       match s.Unix.st_kind with
		   Unix.S_DIR -> t_filename_d
		 | _          -> t_filename
	     in

	     self # t_apply
	       t_directory_row
	       [ "filename",  self # t_apply
 		                 t_fname
				 [ "value", self # t_text name ];
		 "fileperms", self # t_text (permissions
					       s.Unix.st_kind
					       s.Unix.st_perm);
		 "fileowner", self # t_text (owner s.Unix.st_uid);
		 "filegroup", self # t_text (group s.Unix.st_gid);
		 "filesize",  self # t_text (string_of_int s.Unix.st_size);
		 "filemtime", self # t_text (timestring s.Unix.st_mtime);
		 "filesymlink",
		   match ls.Unix.st_kind with
		       Unix.S_LNK ->
			 self # t_apply_byname "symlink"
			   [ "value",
			     self # t_text (Unix.readlink path_name) ]
		     | _ -> self # t_empty;
	       ]
	  )
	  names
      in
      let t1 = Unix.gettimeofday() in

      (* Concatenate 'rows' and put result into 'listing': *)
      self # put_tree "listing" (self # t_concat self#t_empty rows)

    method handle() =
      match self # event with
	  Indexed_button ("goto_dir", dirname) ->
	    begin match dirname with
		"." -> ()
	      | ".." ->
		  let p = Filename.dirname
			    (self # string_variable "path") in
		  self # set_variable "path" (String_value p)
	      | _ ->
		  self # set_variable "path"
		    (String_value
		       (Filename.concat
			  (self # string_variable "path")
			  dirname))
	    end
	| Indexed_button("set_language", lang) ->
	    self # set_variable "lang" (String_value lang)
	| _ -> ()
  end
;;


run
  ~reg:(fun universe ->
          universe # register "directory" (new directory)
       )
  ()
;;


(* ======================================================================
 * History:
 *
 * $Log: index.ml,v $
 * Revision 3.1  2002-02-12 20:51:06  stolpmann
 * 	Initial revision at sourceforge
 *
 * Revision 1.3  2002/02/05 18:51:51  gerd
 * 	Updated.
 *
 * Revision 1.2  2002/01/14 15:06:48  gerd
 * 	Updated to recent wd version.
 *
 * Revision 1.1  2000/04/13 17:43:03  gerd
 * 	Initial revision.
 *
 *
 *)
