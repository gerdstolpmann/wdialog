
(* An example: Add two numbers and display the result. This version of the
 * example uses the FastCGI protocol instead of CGI. It also uses in memory
 * sessions, a secure (and fast) alternative to the instant session manager
 * which is only avaliable for FastCGI and jserv (because the daemon must be
 * memory residant).
 *)

open Wd_dialog
open Wd_run_fcgi
open Netcgi
open Netcgi_fcgi
open Netcgi_types
open Wd_types
open Session

class add_numbers universe name env =
object (self)
  inherit dialog universe name env

  method prepare_page() =
    (* This method is empty in this example *)
    ()

  method handle() =
    (* Check which event has happened: *)
    match self # event with
	Button("add") ->
	  (* Get the numbers and convert them from string to int. Catch
	   * errors.
	   *)
	  let n_1, n_2 =
	    ( try
		let s_1 = self # string_variable "first_number" in
		let s_2 = self # string_variable "second_number" in
		(int_of_string s_1, int_of_string s_2)
	      with
		  error ->
		    (* On error: Jump to the error page *)
		    raise(Change_page "display-error")
	    )
	  in
	  (* Add the numbers, and store the result into the variable *)
	  let r = n_1 + n_2 in
	  self # set_variable "result" (String_value (string_of_int r));

      | _ ->
	  (* Do nothing if the event is not recognized *)
	  ()
end
;;


prerr_endline("Args: " ^ String.concat ";" (Array.to_list Sys.argv));

let reg universe =
  (* Bind the XML dialog "add-numbers" to the class "add_numbers": *)
  universe # register "add-numbers" (new add_numbers);
  prerr_endline "(Initializing...)";
  prerr_endline ("cwd=" ^ Sys.getcwd())
in
  Netcgi_fcgi.serv 
    (Wd_run_fcgi.create_request_handler
       ~session_manager: (new memory_session_manager)
       ~charset:`Enc_utf8 ~reg ~uifile:"adder.ui" ())
    buffered_transactional_optype
