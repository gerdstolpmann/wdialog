open Wd_dialog
open Wd_types

let _ = Random.self_init

let random_char () = 
  if Random.bool () then
    Char.chr ((Random.int 26) + 65)
  else
    Char.chr ((Random.int 26) + 97)

let make_session_id () =
  let length = 128 + (Random.int 128) in
  let id = String.create length in
    for i=0 to length - 1
    do
      id.[i] <- random_char ()
    done;
    id

exception Session_not_found

class memory_session (id: string) (instant_session: session_type) =
object
  val id = id
  val instant_session = instant_session
  method commit_changes () = ()
  method serialize = id
  method change_dialog dlg = instant_session#change_dialog dlg
  method dialog = instant_session#dialog
  method dialog_name = instant_session#dialog_name
end;;

class memory_session_manager =
object
  inherit instant_session_manager () as super
  val sessions = Hashtbl.create 500

  method create dlg = 
    let instant_session = super#create dlg in
    let id = make_session_id () in
    let memses = new memory_session id instant_session in
      Hashtbl.replace sessions id memses;
      memses

  method unserialize universe env id = 
    try Hashtbl.find sessions id
    with Not_found -> raise Session_not_found
end;;
